/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Copy Bitstrings of arbitrary bit alignment and length.
*/

#include "beedb.h"
#include "bitcopy.h"
#include "packed.h"


void
bitcopy_aligned(uint64_t *dst, uint64_t *src, uint64_t wordcount)
{
  inline_bitcopy_aligned(dst, src, wordcount);
}

void
bitcopy_aligned_src(uint64_t *dst_base, uint64_t dst_pos, uint64_t *src,
                    uint64_t numbits)
{
  inline_bitcopy_aligned_src(dst_base, dst_pos, src, numbits);
}

void
bitcopy_aligned_dst(uint64_t *dst, uint64_t *src_base, uint64_t src_pos,
                    uint64_t numbits)
{
  return inline_bitcopy_aligned_dst(dst, src_base, src_pos, numbits);
}

/*
  For the little-endian version, bit positions count from least significant
  bit up, so position 0 is 0/1, position 1 is 0/2, position 2 is 0/4, and so
  on.

  This way, bit positions 0-7 occupy the first byte.
*/

/*
  Copy bit string of size NUMBITS, at arbitrary bit alignment.

  Size to copy must be > 0.

  Any remaining bits in the last 64-bit word to be copied to destination are
  undefined.
*/
void
bitcopy(uint64_t *dst_base, uint64_t dst_pos,
        uint64_t *src_base, uint64_t src_pos,
        uint64_t numbits)
{
  uint64_t *dst= dst_base + (dst_pos >> 6);
  uint64_t dst_word_pos= dst_pos & 0x3f;
  if (dst_word_pos == 0)
    return inline_bitcopy_aligned_dst(dst, src_base, src_pos, numbits);

  uint64_t *src= src_base + (src_pos >> 6);
  uint64_t src_word_pos= src_pos & 0x3f;
  if (src_word_pos == 0)
    return inline_bitcopy_aligned_src(dst_base, dst_pos, src, numbits);

  /*
    Here we know that neither source nor destination are 64-bit aligned,
    avoiding some potential undefined-shift-by-64-bit problems.
  */
  uint64_t dst_bits_in_first= 64 - dst_word_pos;
  /* Preserve bits in first destination word that should not be overwritten. */
  uint64_t d= *dst & (~(uint64_t)0 >> dst_bits_in_first);
  /*
    Special case of same alignment. In this case we do not want any shifting
    (and cannot due to undefined shift-by64-bit).
  */
  if (dst_word_pos == src_word_pos)
  {
    uint64_t v= *src++;
    uint64_t* dst_end= dst + ((dst_word_pos + numbits + 0x3f) >> 6);
    d|= v & (~(uint64_t)0 << dst_word_pos);
    *dst++= d;
    while (dst < dst_end)
    {
      *dst++= *src++;
    }
  }
  else
  {
    /*
      Now check if there are sufficient bits in the first source word to fill
      up the fractional first destination word. If so, take them, use them to
      fill up the first destination word, and use the rest as the first part
      of next destination word. If not, use as much as is there to prepare the
      first part of the first destination word.
    */
    if (dst_word_pos > src_word_pos)
    {
      /*
        We can fill the first destination word completely from the first
        source word (with bits to spare for the second destination word, if
        any).
      */

      uint64_t *src_end= src + ((src_word_pos + numbits + 0x3f) >> 6);
      uint64_t v= *src++;
      /* Number of bits in last, fractional destination word (if any). */
      uint64_t bits_remaining= (dst_word_pos + numbits) & 0x3f;
      d|= (v >> src_word_pos) << dst_word_pos;
      uint64_t shift1= dst_word_pos - src_word_pos;
      uint64_t shift2= dst_bits_in_first + src_word_pos;
      *dst++= d;

      while (src < src_end)
      {
        d= v >> shift2;
        v= *src++;
        d|= v << shift1;
        *dst++= d;
      }

      if (bits_remaining > 0 && bits_remaining <= shift1)
        *dst= v >> shift2;
    }
    else /* dst_word_pos < src_word_pos */
    {
      /*
        To fill up the first destination word, we need bits from both the
        first and the second source word.
      */

      /* Number of bits in last, fractional destination word (if any). */
      uint64_t bits_remaining= (dst_word_pos + numbits) & 0x3f;
      /* dst_end points to just past the last whole destination word. */
      uint64_t *dst_end= dst + ((dst_word_pos + numbits) >> 6);
      uint64_t v= *src++;
      d|= (v >> src_word_pos) << dst_word_pos;
      uint64_t shift1= src_word_pos - dst_word_pos;
      uint64_t shift2= 64 - shift1;

      while (dst < dst_end)
      {
        v= *src++;
        d|= v << shift2;
        *dst++= d;
        d= v >> shift1;
      }

      if (bits_remaining > 0)
      {
        if (bits_remaining > shift2)
          d|= (*src) << shift2;
        *dst= d;
      }
    }
  }
}

/*
  Hm, an alternative implementation using pack() and unpack().

  I actually kind of like this one better, so benchmark and select this one if
  performace is not significantly worse. It is soo much simpler.
*/
void
bitcopy2(uint64_t *dst_base, uint64_t dst_pos,
         uint64_t *src_base, uint64_t src_pos,
         uint64_t numbits)
{
  pack_overwriteptr dst(dst_base, dst_pos);
  uint64_t src_word_pos= src_pos & 0x3f;
  uint64_t bits_in_first= 64 - src_word_pos;
  uint64_t *src= src_base + (src_pos >> 6);
  if (bits_in_first >= numbits)
  {
    /* Everything available in first source word. */
    dst.putbits(numbits, *src >> src_word_pos);
  }
  else
  {
    dst.putbits(bits_in_first, (*src++) >> src_word_pos);
    uint64_t end_bit_pos= src_pos + numbits;
    uint64_t *src_end= src_base + (end_bit_pos >> 6);
    while (src < src_end)
      dst.putbits(64, *src++);
    uint64_t remain_bits= end_bit_pos & 0x3f;
    if (remain_bits > 0)
      dst.putbits(remain_bits, *src & (~(uint64_t)0 >> (64 - remain_bits)));
  }
  dst.flush();
}

void
bitcopy3(uint64_t *dst_base, uint64_t dst_pos,
         uint64_t *src_base, uint64_t src_pos,
         uint64_t numbits)
{
  pack_overwriteptr dst(dst_base, dst_pos);
  uint64_t src_word_pos= src_pos & 0x3f;
  uint64_t bits_in_first= 64 - src_word_pos;
  uint64_t *src= src_base + (src_pos >> 6);
  if (bits_in_first >= numbits)
  {
    /* Everything available in first source word. */
    dst.putbits(numbits, *src >> src_word_pos);
  }
  else
  {
    dst.putbits(bits_in_first, (*src++) >> src_word_pos);
    uint64_t end_bit_pos= src_pos + numbits;
    uint64_t *src_end= src_base + (end_bit_pos >> 6);
    while (src < src_end)
      dst.putbits64(*src++);
    uint64_t remain_bits= end_bit_pos & 0x3f;
    if (remain_bits > 0)
      dst.putbits(remain_bits, *src & (~(uint64_t)0 >> (64 - remain_bits)));
  }
  dst.flush();
}
