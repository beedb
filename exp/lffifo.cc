/*
  Copyright 2008-2009 Kristian Nielsen

  This file is part of BeeDB.

  BeeDB is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  BeeDB is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with BeeDB.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Lock-free FIFO buffers, for experimenting with the cost of memory barriers.
*/

#include <stdint.h>

#define CACHE_LINE_SIZE 64

static inline void rmb(void)
{
#ifdef NO_GCC_BUILTIN_PRIMITIVES
  __asm__ volatile("lfence" : /* No outputs */ : /* No inputs */ : "memory");
#else
  __builtin_ia32_lfence();
#endif
}

static inline void wmb(void)
{
#ifdef NO_GCC_BUILTIN_PRIMITIVES
  __asm__ volatile("sfence" : /* No outputs */ : /* No inputs */ : "memory");
#else
  __builtin_ia32_sfence();
#endif
}

static inline void mb(void)
{
#ifdef NO_GCC_BUILTIN_PRIMITIVES
  __asm__ volatile("mfence" : /* No outputs */ : /* No inputs */ : "memory");
#else
  __builtin_ia32_mfence();
#endif
}

/* cmb() is a compiler barrier. */
static inline void cmb(void)
{
  __asm__ volatile("" : /* No outputs */ : /* No inputs */ : "memory");
}

/* Relax the cpu in spin loop, helps on eg. Pentium. */
static inline void cpu_pause(void)
{
  /*
    The "memory" clobber attribute makes this act as a compiler barrier.
    This is useful, as we will probably be using this in a loop where we will
    be reading some value repeatedly, waiting for it to change. And we need
    in any case to tell the compiler that that value cannot be cached across
    loop iterations.
    In effect, cpu_pause is a synchronisation mechanism, saying "wait for
    memory to change".
  */
  __asm__ volatile("pause" : /* No outputs */ : /* No inputs */ : "memory");
}

/*
  NOTE: Assume that aligned 64-bit writes are atomic. This may or may not be
  the case on 32-bit processors.
*/

class lffifo {
public:
  /*
    Common data.
  */
  union {
    struct {
      uint64_t *buffer;
      uint64_t buffer_size;
    };
    /*
      Make sure the shared data following this gets into a different cache
      line than any data stored before this structure.

      This would not be necessary if we alternatively chose to require the
      structure to be aligned on a cache-line boundary.
    */
    unsigned char padding0[CACHE_LINE_SIZE];
  };
  /*
    Start and end of available data.

    start_pos is first not consumed entry.
    end_pos is first entry not yet written.
    An empty fifo has start_pos==end_pos. This means that there will be one
    entry unused in a full fifo (the one pointed to by end_pos, where
    end_pos+1 == start_pos).

    These pointers are shared between producer and consumer. Therefore, they
    are padded with the cache size, to avoid cache line bouncing from writes
    of unrelated data.
  */
  union {
    /* Updated by reader, read by writer. */
    uint64_t start_pos;
    unsigned char padding1[CACHE_LINE_SIZE];
  };
  union {
    /* Updated by writer, read by reader. */
    uint64_t end_pos;
    unsigned char padding2[CACHE_LINE_SIZE];
  };
  /* Data used only by reader. */
  union {
    struct {
      uint64_t rd_start_pos;
      uint64_t rd_end_pos;                      // Local copy for reader
      uint64_t rd_spin_loops;
    };
    unsigned char padding3[CACHE_LINE_SIZE];
  };
  /* Data used only by writer. */
  union {
    struct{
      uint64_t wr_start_pos;                    // Local copy for writer
      uint64_t wr_end_pos;
      uint64_t wr_spin_loops;
    };
    unsigned char padding4[CACHE_LINE_SIZE];
  };

  /* Constructor. */
  lffifo(uint64_t size)
  {
    buffer= new uint64_t[size];
    if (!buffer)
    {
      fprintf(stderr, "Error: Failed to allocate lffifo buffer (%ld bytes)\n",
              (long)(size*sizeof(uint64_t)));
      exit(1);
    }
    buffer_size= size;
    start_pos= 0;
    end_pos= 0;
    rd_start_pos= wr_start_pos= start_pos;
    rd_end_pos= wr_end_pos= end_pos;
    reset_stats();
  }
  /* Destructor. */
  ~lffifo()
  {
    delete [] buffer;
  }

  /*
    Put and get methods.

    Use put and get of a single word. We want to test the overhead of the
    barriers. Therefore it makes sence to keep the surrounding work as small
    as possible. In the "real world", overhead would be expected to be
    amortised over more "real" work.
  */

private:
  uint64_t simple_get(void)
  {
    uint64_t v= buffer[rd_start_pos];
    rd_start_pos+= 1;
    if (rd_start_pos >= buffer_size)
      rd_start_pos= 0;
    return v;
  }

public:
  uint64_t get(void)
  {
    /* If data is known to be available, just return it. */
    if (rd_start_pos != rd_end_pos)
      return simple_get();

    /*
      Make sure writer knows we have room avilable before waiting for more data.
      Otherwise we might deadlock due to each end not knowing how far the other
      end has proceeded.
    */
    if (rd_start_pos != start_pos)
      read_flush();

    /*
      Now wait for new data to become available.
      In this example, busy wait.
    */
    for(;;)
    {
      rd_end_pos= end_pos;
      if (rd_start_pos != rd_end_pos)
        break;
      cpu_pause();
      rd_spin_loops++;
    }

    /*
      We need a read memory barrier here, to make sure we do not attempt to
      write any data prior to the visibility of the above read of end_pos
      (which signals that the data is available).
    */
    rmb();
    return simple_get();
  }

  void read_flush(void)
  {
    /*
      Hm, what kind of barrier is needed here?

      In terms of the generated code on the CPU, what we need is to make sure
      that this following flush is not made visible to the writer until all
      previous loads have been satisfied (as otherwise the writer may clobber
      them before we read).

      But this is really just a compiler barrier. Just say that at this point,
      memory may be modified (by the writer thread in this case).
    */
    cmb();
    start_pos= rd_start_pos;
  }

private:
  void simple_put(uint64_t new_pos, uint64_t v)
  {
    buffer[wr_end_pos]= v;
    wr_end_pos= new_pos;
  }

public:
  void put(uint64_t v)
  {
    uint64_t new_end_pos= wr_end_pos + 1;
    uint64_t local_start_pos= wr_start_pos;

    if (new_end_pos >= buffer_size)
      new_end_pos= 0;

    if (new_end_pos != local_start_pos)
      return simple_put(new_end_pos, v);

    /*
      Make sure reader knows we have data avilable before waiting for more room.
      Otherwise we might deadlock due to each end not knowing how far the other
      end has proceeded.
    */
    if (wr_end_pos != end_pos)
      write_flush();

    /*
      Now wait for room to become available.
      In this example, busy wait.
    */
    for(;;)
    {
      local_start_pos= start_pos;
      if (new_end_pos != local_start_pos)
        break;
      cpu_pause();
      wr_spin_loops++;
    }

    /*
      We need a read comiler barrier here, to make sure following stores are
      not initated prior to the code reading the new position from reader.
    */
    cmb();
    wr_start_pos= local_start_pos;
    return simple_put(new_end_pos, v);
  }

  void write_flush()
  {
    /*
      Need a write memory barrier here, to make sure that all stored data is
      visible to the reader before making the following store visible. Since
      the following store allows the reader to access that data.
    */
    wmb();
    end_pos= wr_end_pos;
  }

  void reset_stats()
  {
    wr_spin_loops= 0;
    rd_spin_loops= 0;
  }
};

