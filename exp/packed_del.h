/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Try for a version which does not read more 64-bit words than needed, and
  which maybe reduces the dependency chain length by storing #bits available.

  This actually seems to be quite a bit faster for simple constant-size
  getbits() scan! But it works in big-endian mode (getbits_be() below).
  The little-endian version getbits() is a bit slower than the bit-endian
  on simple constant getbits (but still faster than packed.h). But it is
  slower in unpack1() than packed.h.
  I think the slowdown for little endian is due to misprediction of an
  extra conditional needed in the aligned case (due to undefined
  shift-by-64-bit semantics).
  Also tried rewriting without entra conditional (adding an extra maskoff).
  This makes simple getbits(10) a little bit faster, but unpack1() even
  slower still.
*/

#include <stdint.h>

struct pack_ptr_del {
  uint64_t *b;
  uint64_t v;
  uint i;
  uint bitsleft;

  pack_ptr_del(uint64_t *base);
  uint64_t getbits(uint numbits);
  uint64_t getbits_le(uint numbits);
  void putbits(uint numbits, uint64_t data);
  void putbits_le(uint numbits, uint64_t data);

  uint used() { return i; }

  uint64_t unpack1();
  void pack1(uint64_t v);
  uint64_t unpack2();
  void pack2(uint64_t v);
  uint64_t unpack3();
  void pack3(uint64_t v);
  uint64_t unpack4();
  void pack4(uint64_t v);
};

inline
pack_ptr_del::pack_ptr_del(uint64_t *base)
{
  b= base;
  v= base[0];
  i= 1;
  bitsleft= 64;
}

inline uint64_t
pack_ptr_del::getbits_le(uint numbits)
{
  uint x= 64 - numbits;
  if (bitsleft < numbits)
  {
    uint64_t new_v= b[i++];
    /*
      The extra & guards against shift-by-64-bits (in the bitsleft==0 case),
      which is undefined in C/C++.
    */
    uint64_t r= ((v >> (64 - bitsleft)) & (((uint64_t)1 << bitsleft) - 1)) |
                ((new_v << bitsleft) & (~(uint64_t)0 >> x));
    bitsleft= bitsleft + x;
    v= new_v;
    return r;
  }
  else
  {
    uint64_t r= (v >> (64 - bitsleft)) & ((~(uint64_t)0 >> x));
    bitsleft-= numbits;
    return r;
  }
}

inline uint64_t
pack_ptr_del::getbits(uint numbits)
{
  uint x= 64 - numbits;
  if (bitsleft >= numbits)
  {
    bitsleft-= numbits;
    return((v >> bitsleft) & ((~(uint64_t)0 >> x)));
  }
  else
  {
    uint64_t new_v= b[i++];
    /*
      Guard against 64-bit shift, which is undefined in C/C++.
      But include the test numbits==64, which is often statically computable
      to false by the compiler (constant bit field size). This means that in
      many cases this if statement does not translate into a conditional
      jump.
    */
    if (likely(!(numbits == 64 && bitsleft == 0)))
    {
      uint64_t r= (v << (numbits - bitsleft));
      bitsleft= bitsleft + x;
      v= new_v;
      return((r | (new_v >> bitsleft)) & ((~(uint64_t)0 >> x)));
    }
    else
    {
      return new_v;
    }
  }
}

/*
  Assumes data only has set bits in the first NUMBITS positions, ie. that
  the value has already been masked off to NUMBITS bits if needed.
*/
inline void
pack_ptr_del::putbits(uint numbits, uint64_t data)
{
  if (bitsleft >= numbits)
  {
    uint64_t mask= ~(uint64_t)0 >> (64-numbits);
    bitsleft-= numbits;
    v= (v & ~(mask << bitsleft)) | (data << bitsleft);
    b[i-1]= v;
  }
  else
  {
    uint64_t mask1= ~(uint64_t)0 << bitsleft;
    if (likely(!(numbits == 64 && bitsleft == 0)))
    {
      b[i-1]= (v & mask1) | (data >> (numbits - bitsleft));
      bitsleft= bitsleft + 64 - numbits;
      uint64_t mask2= ~(uint64_t)0 >> (64 - bitsleft);
      v= (b[i] & mask2) | (data << bitsleft);
      b[i++]= v;
    }
    else
    {
      v= b[i++]= data;
    }
  }
}

inline uint64_t
pack_ptr_del::unpack1()
{
  uint n= getbits(3);
  return getbits(n*9+1);
}

inline void
pack_ptr_del::pack1(uint64_t v)
{
  /*
    Compute the 3-bit header counter C, where we store C*9+1 bits.
    We have C = ((#bits - 1) + 8) / 9   [rounding down].
    And #bits = 64 - #leading zeroes, and 64 - 1 + 8 == 71.
    We use |1 to be sure to get at least a size of one bit (__builtin_clzl is
    undefined for argument of zero).
  */
  uint bits= (71 - COUNT_LEADING_ZEROS_64(v | 1))/9;
  putbits(3, bits);
  putbits(bits*9+1, v);
}

inline uint64_t
pack_ptr_del::unpack2()
{
  uint n= getbits(2);
  return getbits(n*20+4);
}

inline void
pack_ptr_del::pack2(uint64_t v)
{
  uint bits= (79 - COUNT_LEADING_ZEROS_64(v | 1))/20;
  putbits(2, bits);
  putbits(bits*20+4, v);
}
