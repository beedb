/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  BeeDB is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  BeeDB is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with BeeDB.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Try for a version which shifts the cached 64-bit word in-place, making
  available the result word without having to compute a shift first,
  hopefully reducing the length of the dependency chain.

  Also use uint64_t throughout, seems to sometimes save the odd instruction
  otherwise needed for sign extension.
*/

#include "bitop.h"

/*
  This was an experiment to pre-fetch one 64-bit word ahead.
  However, in addition to a nasty requirement to keep 8 bytes of extra readable
  memory at the end of data, it also turns out to be about 3% slower!
*/
// #define PREFETCH_EXPERIMENT

/*
  An experiment with using non-temporal writes in pack_overwriteptr.

  This avoid loading the memory being written into cache.
*/
//#define NONTEMPORAL_EXPERIMENT

class pack_ptr_base {
 private:
  /* Base pointer, never modified. */
  uint64_t * const base;
  /* Current value, all but lower BITSLEFT bits are zero. */
  uint64_t v;
#ifdef PREFETCH_EXPERIMENT
  uint64_t v_next;
#endif
  /* Index into BASE of next word to fetch. */
  uint64_t i;
  /* Number of bits left in current word V. */
  uint64_t bitsleft;

 public:
  pack_ptr_base(uint64_t *_base);
  /*
    This constructor sets up to start reading from _bit_ position relative to
    BASE. Ie. we might pass start_bitpos=100 to skip the first 5 20-bit words.
  */
  pack_ptr_base(uint64_t *_base, uint64_t start_bitpos);

  void seek(uint64_t bitpos);
  uint64_t getbits(uint64_t numbits);
  /*
    This method needs to load + store the value at every invocation, due to
    possibility of seek()/getbits() intermixed with putbits().

    For more efficient multiple putbits() in serial, use pack_writeptr or
    pack_overwriteptr.
  */
  void putbits(uint64_t numbits, uint64_t data);
  /* More efficient versions for when numbits==64. */
  uint64_t getbits64();
  void putbits64(uint64_t data);

  uint64_t used_words() { return i; }
  uint64_t used_bits() { return (i<<6) - bitsleft; }
};


inline
pack_ptr_base::pack_ptr_base(uint64_t *_base) :
  base(_base), v(0), i(0), bitsleft(0)
{
#ifdef PREFETCH_EXPERIMENT
  v_next= base[i++];
#endif
  /*
    We do not pre-load the first word, as we use the invariant (bitsleft < 64)
    to avoid a special case for undefined shift-by-64-bit.
  */
}

inline
pack_ptr_base::pack_ptr_base(uint64_t *_base, uint64_t start_bitpos) :
  base(_base)
{
  seek(start_bitpos);
}

inline void
pack_ptr_base::seek(uint64_t bitpos)
{
  /*
    Since we cannot have bitsleft==64, in the case where we start on a word
    boundary we need to point to the previous word and set bitsleft=0.
  */
  uint64_t fraction= bitpos & 63;
  i= bitpos >> 6;
  if (likely(fraction > 0))
  {
    bitsleft= 64 - fraction;
    v= base[i++] >> fraction;
  }
  else
  {
    bitsleft= 0;
    v= 0;
  }
#ifdef PREFETCH_EXPERIMENT
  v_next= base[i++];
#endif
}

inline uint64_t
pack_ptr_base::getbits(uint64_t numbits)
{
  if (bitsleft >= numbits)
  {
    uint64_t r= v & (~(uint64_t)0 >> (64 - numbits));
    /*
      The value of bitsleft is at most 63, so for numbits==64 we never get
      here with an otherwise undefined shift-by-64-bit operation.
    */
    v>>= numbits;
    bitsleft-= numbits;
    return r;
  }
  else
  {
    uint64_t r= v;
#ifdef PREFETCH_EXPERIMENT
    v= v_next;
    v_next= base[i++];
#else
    v= base[i++];
#endif
    r= (r | (v << bitsleft)) & (~(uint64_t)0 >> (64 - numbits));
    /*
      Guard against undefined shift-by-64-bit.
      In many cases the numbits==64 condition will be statically determined
      by the compiler, so no branch will be generated, or in any case the CPU
      should be safe to predict this conditional as true.
    */
    if (likely(!(numbits == 64 && bitsleft == 0)))
      v>>= numbits - bitsleft;
    else
      v= 0;
    bitsleft= bitsleft + (64 - numbits);
    return r;
  }
}

inline uint64_t
pack_ptr_base::getbits64()
{
  uint64_t r= v;
#ifdef PREFETCH_EXPERIMENT
  v= v_next;
  v_next= base[i++];
#else
  v= base[i++];
#endif
  r|= v << bitsleft;
  /* Guard against undefined shift-by-64-bit. */
  if (bitsleft != 0)
    v>>= 64 - bitsleft;
  else
    v= 0;
  return r;
}

inline void
pack_ptr_base::putbits(uint64_t numbits, uint64_t data)
{
  if (bitsleft > 0)
  {
    uint64_t val= base[i-1];
    uint64_t mask= (~(uint64_t)0 >> (64 - numbits)) << (64 - bitsleft);
    val= (val & ~mask) | (data << (64 - bitsleft));
    if (bitsleft >= numbits)
    {
      bitsleft-= numbits;
      v>>= numbits;
    }
    else
    {
      uint64_t rest= numbits - bitsleft;
      bitsleft+= 64 - numbits;
      mask= ~(uint64_t)0 >> bitsleft;
      v= (base[i] & ~mask) | (data >> rest);
      base[i++]= v;
      v>>= rest;
    }
  }
  else
  {
    uint64_t mask= ~(uint64_t)0 >> (64 - numbits);
    v= (base[i] & ~mask) | data;
    base[i++]= v;
    if (numbits != 64)
      v>>= numbits;
    else
      v= 0;
    bitsleft= 64 - numbits;
  }
}

inline void
pack_ptr_base::putbits64(uint64_t data)
{
  if (bitsleft > 0)
  {
    uint64_t val= base[i-1];
    uint64_t mask= ~(uint64_t)0 << (64 - bitsleft);
    val= (val & ~mask) | (data << (64 - bitsleft));
    uint64_t rest= 64 - bitsleft;
    mask= ~(uint64_t)0 >> bitsleft;
    v= (base[i] & ~mask) | (data >> rest);
    base[i++]= v;
    v>>= rest;
  }
  else
  {
    base[i++]= data;
    v= 0;
  }
}


/*
  This template adds pack() methods to a base class that supports putbits().
*/
template <class B>
class templ_pack_writeptr : public B {
 public:
  templ_pack_writeptr(uint64_t *base) : B(base) { }
  /*
    This constructor with specified start position will only be available in
    some instantiations.
  */
  templ_pack_writeptr(uint64_t *base, uint64_t start_bitpos) :
    B(base, start_bitpos) { }

  void pack1(uint64_t v);
  void pack2(uint64_t v);
  void pack3(uint64_t v);
  void pack4(uint64_t v);
  void pack5(uint64_t v);

  /*
    These are used to efficiently pack/unpack signed numbers.
    They convert to a representation where numbers of small magnitude (both
    negative and positive numbers) use few significant bits (most leading bits
    are zero).
  */
  static uint64_t signed_encode(int64_t i);
  static int64_t signed_decode(uint64_t u);
};

/*
  This template adds both pack() and unpack() to a base class that supports
  both putbits() and getbits().
*/
template <class B>
class templ_pack_ptr : public templ_pack_writeptr<B> {
 public:
  templ_pack_ptr(uint64_t *base) : templ_pack_writeptr<B>(base) { }
  templ_pack_ptr(uint64_t *base, uint64_t start_bitpos) :
    templ_pack_writeptr<B>(base, start_bitpos) { }
  uint64_t unpack1();
  uint64_t unpack2();
  uint64_t unpack3();
  uint64_t unpack4();
  uint64_t unpack5();
};

typedef templ_pack_ptr<pack_ptr_base> pack_ptr;


template <class B>
inline void
templ_pack_writeptr<B>::pack1(uint64_t v)
{
  /*
    Compute the 3-bit header counter C, where we store C*9+1 bits.
    We have C = ((#bits - 1) + 8) / 9   [rounding down].
    And #bits = 64 - #leading zeroes, and 64 - 1 + 8 == 71.
    We use |1 to be sure to get at least a size of one bit (__builtin_clzl is
    undefined for argument of zero).
  */
  uint64_t bits= (71 - COUNT_LEADING_ZEROS_64(v | 1))/9;
  B::putbits(3, bits);
  B::putbits(bits*9+1, v);
}

template <class B>
inline uint64_t
templ_pack_ptr<B>::unpack1()
{
  uint64_t n= B::getbits(3);
  return B::getbits(n*9+1);
}

template <class B>
inline void
templ_pack_writeptr<B>::pack2(uint64_t v)
{
  uint64_t bits= (79 - COUNT_LEADING_ZEROS_64(v | 1))/20;
  B::putbits(2, bits);
  B::putbits(bits*20+4, v);
}

template <class B>
inline uint64_t
templ_pack_ptr<B>::unpack2()
{
  uint64_t n= B::getbits(2);
  return B::getbits(n*20+4);
}

#define LOOKUP_PACK_GET_TAG_BITS(v)   ( (v) & 0xffff )
#define LOOKUP_PACK_GET_TAG_VALUE(v)  ( (v) >> 24 )
#define LOOKUP_PACK_GET_DATA_BITS(v)  ( ((v) >> 16) & 0xff )

#define LOOKUP_UNPACK_GET_BITS(v)     ( (v) & 0xffff )
#define LOOKUP_UNPACK_GET_OR_VALUE(v) ( (v) >> 24 )
#define LOOKUP_UNPACK_GET_LSHIFT(v)   ( ((v) >> 16) & 0xff )

extern uint32_t lookup_table_pack3[];
template <class B>
inline void
templ_pack_writeptr<B>::pack3(uint64_t v)
{
  uint bits= COUNT_LEADING_ZEROS_64(v | 1);
  uint32_t lookup_word= lookup_table_pack3[bits];
  uint64_t tag_bits= LOOKUP_PACK_GET_TAG_BITS(lookup_word);
  uint64_t tag_value= LOOKUP_PACK_GET_TAG_VALUE(lookup_word);
  uint64_t data_bits= LOOKUP_PACK_GET_DATA_BITS(lookup_word);
  B::putbits(tag_bits, tag_value);
  B::putbits(data_bits, v);
}

extern uint32_t lookup_table_unpack3[];
template <class B>
inline uint64_t
templ_pack_ptr<B>::unpack3()
{
  uint prefix= B::getbits(3);
  uint32_t lookup_word = lookup_table_unpack3[prefix];
  uint64_t bits= LOOKUP_UNPACK_GET_BITS(lookup_word);
  uint64_t or_value= LOOKUP_UNPACK_GET_OR_VALUE(lookup_word);
#ifndef WORDS_BIGENDIAN
  uint64_t lshift= LOOKUP_UNPACK_GET_LSHIFT(lookup_word);
#endif
  return ( B::getbits(bits)
#ifndef WORDS_BIGENDIAN
           << lshift
#endif
         ) | or_value;
}

extern uint32_t lookup_table_pack4[];
template <class B>
inline void
templ_pack_writeptr<B>::pack4(uint64_t v)
{
  uint bits= COUNT_LEADING_ZEROS_64(v | 1);
  uint32_t lookup_word= lookup_table_pack4[bits];
  uint64_t tag_bits= LOOKUP_PACK_GET_TAG_BITS(lookup_word);
  uint64_t tag_value= LOOKUP_PACK_GET_TAG_VALUE(lookup_word);
  uint64_t data_bits= LOOKUP_PACK_GET_DATA_BITS(lookup_word);
  B::putbits(tag_bits, tag_value);
  B::putbits(data_bits, v);
}

extern uint32_t lookup_table_unpack4[];
template <class B>
inline uint64_t
templ_pack_ptr<B>::unpack4()
{
  uint prefix= B::getbits(3);
  uint32_t lookup_word = lookup_table_unpack4[prefix];
  uint64_t bits= LOOKUP_UNPACK_GET_BITS(lookup_word);
  uint64_t or_value= LOOKUP_UNPACK_GET_OR_VALUE(lookup_word);
#ifndef WORDS_BIGENDIAN
  uint64_t lshift= LOOKUP_UNPACK_GET_LSHIFT(lookup_word);
#endif
  return ( B::getbits(bits)
#ifndef WORDS_BIGENDIAN
           << lshift
#endif
         ) | or_value;
}

#undef LOOKUP_PACK_GET_TAG_BITS
#undef LOOKUP_PACK_GET_TAG_VALUE
#undef LOOKUP_PACK_GET_DATA_BITS

#undef LOOKUP_UNPACK_GET_BITS
#undef LOOKUP_UNPACK_GET_OR_VALUE
#undef LOOKUP_UNPACK_GET_LSHIFT

/*
  Try hard to reduce the length of the dependency chain:
   - Don't use mul/div, as they have high latency.
   - Very simple computation of first part of value (3 ops) to avoid
     initial stall.
   - Compute second part independently from first for parallelism.

  Method: 4 bits of header, 4, 8, 12, ..., 64 bits of data, total 8-68 bits.

  This is significantly faster (maybe 33% or so) than pack[1-4].
*/
template <class B>
inline void
templ_pack_writeptr<B>::pack5(uint64_t v)
{
  uint64_t highest_bit;
  asm( "bsrq %1, %0" : "=r" (highest_bit) : "r" (v|1) : "cc");
  uint64_t bits1= highest_bit>>2;
  uint64_t bits2= (highest_bit & 0x3c) + 4;
  B::putbits(4, bits1);
  B::putbits(bits2, v);
}

template <class B>
inline uint64_t
templ_pack_ptr<B>::unpack5()
{
  uint64_t n= B::getbits(4);
  return B::getbits((n*4) + 4);
}

/*
  Encoding signed numbers as unsigned for pack/unpack, with small number of
  significant digits for numbers of small magnitude.

  Method is to store the sign bit in bit 0, and in bits 1-63 store x (for
  non-negative x) respectively -(x+1) (for negative x).

  The asymmetry allows representing _all_ numbers exactly and avoids
  representing -0.

  Binary encoding of 0, 1, 2, 3: 000, 010, 100, 110
  Binary encoding of -1, -2, -3: 001, 011, 101
  Decoded values by increasing encoding: 0, -1, 1, -2, 2, -3, 3, ...

  The encoding of signed i as unsigned u and vise versa can be descripted thus:

    u = i >= 0 ? (i << 1) : (1 | ((-(i+1))<<1))
    i = (u & 1) ? (-(u>>1) - 1) : (u>>1)

  However, the implementation uses clever bit manipulations to get the same
  results with fewer operations and no conditionals. The dependency chain
  length for encode is only 2, and for decode 3 operations.

  The main idea is that for signed number i, (i>>63) is either 0 or
  0xffffffffffffffff depending on sign; and (i^0xffffffffffffffff) = -(i+1).

  ToDo: Hm, signed shift right is actually implementation dependent in
  C/C++ :-(. So need some port/*.h to replace (i>>63) with -(i < 0).
*/
template <class B>
inline uint64_t
templ_pack_writeptr<B>::signed_encode(int64_t i)
{
  // return (((uint64_t)i ^ (uint64_t)(i>>63)) << 1) | (uint64_t)i >> 63;
  return ((uint64_t)i << 1) ^ (uint64_t)(i >> 63);
}

template <class B>
inline int64_t
templ_pack_writeptr<B>::signed_decode(uint64_t u)
{
  return (int64_t) ( (u >> 1) ^ (uint64_t)((int64_t)(u<<63) >> 63) );
}



/*
  Class for stream writing bit-aligned values of arbitrary bit-size <= 64.
  This class is optimized for writing new data sequentially, overwriting
  any previous values. This way it does not need to to any loads from the
  destination buffer at all.
*/
class pack_overwriteptr_base {
 private:
  uint64_t *const base;
  uint64_t v;
  uint64_t i;
  uint64_t bitpos;

 public:
  pack_overwriteptr_base(uint64_t *_base);
  pack_overwriteptr_base(uint64_t *_base, uint64_t start_bitpos);

  /*
    Write bits into buffer.
    It is necessary to call flush() after the last write to ensure that any
    final fractional 64-bit destination word is stored into buffer. This way
    the putbits() call is faster since it does not need to execute a store
    at every invocation.
  */
  void putbits(uint64_t numbits, uint64_t data);
  /* Special version to optimize writing a full 64-bit word. */
  void putbits64(uint64_t data);
  void flush();
  uint64_t used_words() { return i + (bitpos > 0); }
  uint64_t used_bits() { return (i<<6) + bitpos; }
};

typedef templ_pack_writeptr<pack_overwriteptr_base> pack_overwriteptr;

inline
pack_overwriteptr_base::pack_overwriteptr_base(uint64_t *_base) :
  base(_base), v(0), i(0), bitpos(0)
{
}

inline
pack_overwriteptr_base::pack_overwriteptr_base(uint64_t *_base,
                                               uint64_t start_bitpos) :
  base(_base), bitpos(start_bitpos & 0x3f)
{
  i= start_bitpos >> 6;
  if (bitpos > 0)
  {
    v= base[i] & (~(uint64_t)0 >> (64 - bitpos));
  }
  else
  {
    v= 0;
  }
}

inline void
pack_overwriteptr_base::putbits(uint64_t numbits, uint64_t data)
{
  uint64_t old_bitpos= bitpos;
  v|= data << old_bitpos;
  bitpos= old_bitpos + numbits;
  if (bitpos >= 64)
  {
#ifdef NONTEMPORAL_EXPERIMENT
    STORE_NON_TEMPORAL_64(base + i, v);
    i++;
#else
    base[i++]= v;
#endif
    bitpos-= 64;
    if (likely(!(numbits == 64 && old_bitpos == 0)))
      v= data >> (64 - old_bitpos);
    else
      v= 0;
  }
}

inline void
pack_overwriteptr_base::putbits64(uint64_t data)
{
  v|= data << bitpos;
#ifdef NONTEMPORAL_EXPERIMENT
  STORE_NON_TEMPORAL_64(base + i, v);
  i++;
#else
  base[i++]= v;
#endif
  if (bitpos != 0)
    v= data >> (64 - bitpos);
  else
    v= 0;
}

inline void
pack_overwriteptr_base::flush()
{
  if (bitpos > 0) {
#ifdef NONTEMPORAL_EXPERIMENT
    STORE_NON_TEMPORAL_64(base + i, v);
#else
    base[i]= v;
#endif
  }
}
