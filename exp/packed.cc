/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "beedb.h"
#include "packed.h"

/*
  These tables are used for pack3() and pack4() methods.

  We pack the information into a single 32-bit word in order to be able to
  read them with a single load (and to reduce cache footprint), as this is
  accessed on the critical path performance-wise.
*/

/*
  The pack3() / unpack3() methods store data as follows for big/little endian:

  BE: 0xxx      LE:    xxx0
      10<11x>       <11x>01
      110<20x>     <20x>011
      111<64x>     <64x>111

  So 4, 13, 23, or 67 bits.

  The pack4() / unpack4() methods:

  BE: 00xx      LE:    xx00
      01<8x>         <8x>01
      10<16x>       <16x>10
      110<24x>     <24x>011
      111<64x>     <64x>111

  So 4, 10, 18, 27, or 67 bits.
*/

#define LOOKUP_PACK_ENTRY(tag_bits, data_bits, tag_value) \
        ( (tag_bits) | ((tag_value) << 24) | ((data_bits) << 16) )
#ifdef WORDS_BIGENDIAN
#define LOOKUP_UNPACK_ENTRY(bits, or_value)  ( (bits) | ((or_value) << 24) )
#else
#define LOOKUP_UNPACK_ENTRY(bits, or_value, lshift)  \
        ( (bits) | ((or_value) << 24) | ((lshift) << 16) )
#endif

uint32_t lookup_table_unpack3[8] = {
#ifdef WORDS_BIGENDIAN
  /* 0 */ LOOKUP_UNPACK_ENTRY(1, 0),
  /* 1 */ LOOKUP_UNPACK_ENTRY(1, 2),
  /* 2 */ LOOKUP_UNPACK_ENTRY(1, 4),
  /* 3 */ LOOKUP_UNPACK_ENTRY(1, 6),
  /* 4 */ LOOKUP_UNPACK_ENTRY(10, 0),
  /* 5 */ LOOKUP_UNPACK_ENTRY(10, 1024),
  /* 6 */ LOOKUP_UNPACK_ENTRY(20, 0),
  /* 7 */ LOOKUP_UNPACK_ENTRY(64, 0)
#else
  /* 0 */ LOOKUP_UNPACK_ENTRY( 1, 0, 2),
  /* 1 */ LOOKUP_UNPACK_ENTRY(10, 0, 1),
  /* 2 */ LOOKUP_UNPACK_ENTRY( 1, 1, 2),
  /* 3 */ LOOKUP_UNPACK_ENTRY(20, 0, 0),
  /* 4 */ LOOKUP_UNPACK_ENTRY( 1, 2, 2),
  /* 5 */ LOOKUP_UNPACK_ENTRY(10, 1, 1),
  /* 6 */ LOOKUP_UNPACK_ENTRY( 1, 3, 2),
  /* 7 */ LOOKUP_UNPACK_ENTRY(64, 0, 0)
#endif
};

uint32_t lookup_table_pack3[64] = {
#ifdef WORDS_BIGENDIAN
  /* 64 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 63 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 62 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 61 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 60 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 59 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 58 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 57 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 56 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 55 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 54 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 53 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 52 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 51 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 50 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 49 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 48 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 47 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 46 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 45 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 44 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 43 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 42 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 41 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 40 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 39 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 38 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 37 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 36 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 35 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 34 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 33 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 32 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 31 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 30 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 29 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 28 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 27 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 26 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 25 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 24 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 23 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 22 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 21 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 20 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 19 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 18 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 17 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 16 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 15 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 14 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 13 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 12 */ LOOKUP_PACK_ENTRY(3, 20, 6),
  /* 11 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /* 10 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  9 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  8 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  7 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  6 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  5 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  4 */ LOOKUP_PACK_ENTRY(2, 11, 2),
  /*  3 */ LOOKUP_PACK_ENTRY(1,  3, 0),
  /*  2 */ LOOKUP_PACK_ENTRY(1,  3, 0),
  /*  1 */ LOOKUP_PACK_ENTRY(1,  3, 0)
#else
  /* 64 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 63 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 62 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 61 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 60 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 59 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 58 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 57 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 56 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 55 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 54 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 53 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 52 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 51 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 50 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 49 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 48 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 47 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 46 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 45 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 44 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 43 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 42 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 41 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 40 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 39 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 38 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 37 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 36 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 35 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 34 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 33 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 32 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 31 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 30 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 29 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 28 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 27 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 26 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 25 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 24 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 23 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 22 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 21 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 20 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 19 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 18 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 17 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 16 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 15 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 14 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 13 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 12 */ LOOKUP_PACK_ENTRY(3, 20, 3),
  /* 11 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /* 10 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  9 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  8 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  7 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  6 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  5 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  4 */ LOOKUP_PACK_ENTRY(2, 11, 1),
  /*  3 */ LOOKUP_PACK_ENTRY(1,  3, 0),
  /*  2 */ LOOKUP_PACK_ENTRY(1,  3, 0),
  /*  1 */ LOOKUP_PACK_ENTRY(1,  3, 0)
#endif
};

uint32_t lookup_table_unpack4[8] = {
#ifdef WORDS_BIGENDIAN
  /* 0 */ LOOKUP_UNPACK_ENTRY(1, 0),
  /* 1 */ LOOKUP_UNPACK_ENTRY(1, 2),
  /* 2 */ LOOKUP_UNPACK_ENTRY(7, 0),
  /* 3 */ LOOKUP_UNPACK_ENTRY(7, 128),
  /* 4 */ LOOKUP_UNPACK_ENTRY(15, 0),
  /* 5 */ LOOKUP_UNPACK_ENTRY(15, 32768),
  /* 6 */ LOOKUP_UNPACK_ENTRY(24, 0),
  /* 7 */ LOOKUP_UNPACK_ENTRY(64, 0)
#else
  /* 0 */ LOOKUP_UNPACK_ENTRY(1, 0, 1),
  /* 1 */ LOOKUP_UNPACK_ENTRY(7, 0, 1),
  /* 2 */ LOOKUP_UNPACK_ENTRY(15, 0, 1),
  /* 3 */ LOOKUP_UNPACK_ENTRY(24, 0, 0),
  /* 4 */ LOOKUP_UNPACK_ENTRY(1, 1, 1),
  /* 5 */ LOOKUP_UNPACK_ENTRY(7, 1, 1),
  /* 6 */ LOOKUP_UNPACK_ENTRY(15, 1, 1),
  /* 7 */ LOOKUP_UNPACK_ENTRY(64, 0, 0)
#endif
};

uint32_t lookup_table_pack4[64] = {
#ifdef WORDS_BIGENDIAN
  /* 64 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 63 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 62 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 61 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 60 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 59 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 58 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 57 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 56 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 55 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 54 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 53 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 52 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 51 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 50 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 49 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 48 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 47 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 46 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 45 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 44 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 43 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 42 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 41 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 40 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 39 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 38 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 37 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 36 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 35 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 34 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 33 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 32 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 31 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 30 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 29 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 28 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 27 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 26 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 25 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 24 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 23 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 22 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 21 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 20 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 19 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 18 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 17 */ LOOKUP_PACK_ENTRY(3, 24, 6),
  /* 16 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 15 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 14 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 13 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 12 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 11 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 10 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /*  9 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /*  8 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  7 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  6 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  5 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  4 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  3 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  2 */ LOOKUP_PACK_ENTRY(2,  2, 0),
  /*  1 */ LOOKUP_PACK_ENTRY(2,  2, 0),
#else
  /* 64 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 63 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 62 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 61 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 60 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 59 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 58 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 57 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 56 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 55 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 54 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 53 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 52 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 51 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 50 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 49 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 48 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 47 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 46 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 45 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 44 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 43 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 42 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 41 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 40 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 39 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 38 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 37 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 36 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 35 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 34 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 33 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 32 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 31 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 30 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 29 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 28 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 27 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 26 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 25 */ LOOKUP_PACK_ENTRY(3, 64, 7),
  /* 24 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 23 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 22 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 21 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 20 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 19 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 18 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 17 */ LOOKUP_PACK_ENTRY(3, 24, 3),
  /* 16 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 15 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 14 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 13 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 12 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 11 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /* 10 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /*  9 */ LOOKUP_PACK_ENTRY(2, 16, 2),
  /*  8 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  7 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  6 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  5 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  4 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  3 */ LOOKUP_PACK_ENTRY(2,  8, 1),
  /*  2 */ LOOKUP_PACK_ENTRY(2,  2, 0),
  /*  1 */ LOOKUP_PACK_ENTRY(2,  2, 0),
#endif
};
