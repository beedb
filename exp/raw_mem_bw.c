/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

typedef int v4si __attribute__ ((vector_size (16)));
typedef float v4sf __attribute__ ((vector_size (16)));

#if 0
void
load_loop(float *p, uint64_t count)
{
  if (0xf & (unsigned long)p)
  {
    fprintf(stderr, "load_loop: error: unaligned source\n");
    exit(1);
  }
  while (count > 0)
  {
    v4sf x0= __builtin_ia32_loadaps(p);
    v4sf x1= __builtin_ia32_loadaps(p+4);
    v4sf x2= __builtin_ia32_loadaps(p+8);
    v4sf x3= __builtin_ia32_loadaps(p+12);
  }
}
#endif

void
foo(int *p)
{
  v4si x= *(v4si *)p;
  v4si y= *(v4si *)(p+4);
  *(v4si *)(p+4)= x;
  *(v4si *)(p)= y;
}

void
load_loop(v4si *p, uint64_t count)
{
  if (0xf & (unsigned long)p)
  {
    fprintf(stderr, "load_loop: error: unaligned source\n");
    exit(1);
  }
  v4si x;
  while (count-- > 0)
  {
    x+= p[0];
    x+= p[1];
    x+= p[2];
    x+= p[3];
    p+= 4;
  }
  *p= x;
}

int
main(int argc, char *argv)
{
  uint64_t count= 1024*1024*16;
  uint64_t loops= 10;
  v4si *p= malloc((count+1)*64);
  while (loops--)
  {
    load_loop(p, count);
  }
  printf("%d\n", *(char *)(p+count));
}
