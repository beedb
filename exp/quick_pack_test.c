#include <stdint.h>
#include <stdio.h>

/*
  Test using asm to get best instructions for count-significant-bits and
  divide-by-multiply. Turns out that especially multiply has a high latency
  (3-8 cycles depending apparently), so not optimal to use for
  pack()/unpack().
*/

#define NOINLINE __attribute__((noinline))

NOINLINE static uint64_t
get_num_bits(uint64_t v) {
  uint64_t v2= v|1;
  uint64_t v3;
  asm( "bsrq %1, %0" : "=r" (v3) : "r" (v2) : "cc");
  uint64_t v4= v3 + 8;
  register uint64_t v5 asm("rax") = v4;
  register uint64_t v6 asm ("rdx") = 0x1c71c71d00000000;
  asm( "mulq %0" : "=r" (v6), "=r" (v5) : "r" (v5), "r" (v6));
  return v6;
}

int
main(int argc, char *argv[]) {
  uint64_t i;
  for (i = 0; i <= 64; i++) {
    uint64_t v;
    if (i == 0) {
      v = 0;
    } else {
      v = (uint64_t)1 << (i-1);
    }
    printf("%lu\t%16lx\t%lu\n", i, v, get_num_bits(v));
  }
  return 0;
}
