#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "port/format_macros.h"


uint64_t pagesize = 1024;

enum node_header_flags {
  LEAF_NODE = 1
};

struct node_header {
  uint32_t used_length;
  unsigned char flags;
  unsigned char data[];
};


class btree_key {
public:
  /* Return -1,0,1 if this <,==,> other */
  virtual int compare(const btree_key *other) const = 0;
  /* Cast a pointer-to-memory into an instance of our concrete type. */
  virtual btree_key *from_memory(unsigned char **mem) const = 0;

  virtual uint32_t size() const = 0;
  /* The default implementation here works when size is fixed. */
  virtual unsigned char *skip_memory(unsigned char *mem) const
    { return mem + size(); }
  virtual void to_memory(unsigned char *mem) const = 0;

  virtual ~btree_key() { };
};

/* Instance of btree_key used for testing. */
class my_key : public btree_key {
public:
  my_key(uint64_t _k1, uint64_t _k2);
  int compare(const btree_key *other) const;
  btree_key *from_memory(unsigned char **mem) const;
  uint32_t size() const;
  void to_memory(unsigned char *mem) const;
  ~my_key();

  uint64_t k1;
  uint64_t k2;
};

int
my_key::compare(const btree_key * other_generic) const {
  const my_key *other= (const my_key *)other_generic;
  if (this->k1 < other->k1)
    return -1;
  else if (this->k1 > other->k1)
    return 1;
  else if (this->k2 < other->k2)
    return -1;
  else if (this->k2 > other->k2)
    return 1;
  else
    return 0;
}

btree_key *
my_key::from_memory(unsigned char **mem) const
{
  uint64_t v1, v2;
  memcpy(&v1, *mem, sizeof(uint64_t));
  memcpy(&v2, *mem + sizeof(uint64_t), sizeof(uint64_t));
  *mem= *mem + 2*sizeof(uint64_t);
  return new my_key(v1, v2);
}

uint32_t
my_key::size() const
{
  return 2*sizeof(uint64_t);
}

void
my_key::to_memory(unsigned char *mem) const
{
  memcpy(mem, &k1, sizeof(uint64_t));
  memcpy(mem + sizeof(uint64_t), &k2, sizeof(uint64_t));
}

my_key::my_key(uint64_t _k1, uint64_t _k2) :
  k1(_k1), k2(_k2)
{
  /* Nothing. */
}

my_key::~my_key()
{
  /* Nothing. */
}


/* Get a new, empty BTree. Deallocate with btree_free(). */
node_header *
btree_get_empty()
{
  node_header *node= (node_header *)malloc(pagesize);
  node->used_length= 0;
  node->flags= LEAF_NODE;
  return node;
}

void
btree_free(node_header *node, btree_key *dummy_key)
{
  if (!(node->flags & LEAF_NODE))
  {
    /* Free all child nodes of non-leaf node. */
    unsigned char *p= node->data;
    unsigned char *end_point= p + node->used_length;
    node_header *child;
    for (;;)
    {
      memcpy(&child, p, sizeof(node_header *));
      btree_free(child, dummy_key);
      p+= sizeof(node_header *);
      if (p >= end_point)
        break;
      p= dummy_key->skip_memory(p);
    }
  }

  free(node);
}

/* Returned key must be freed with delete(). */
static btree_key *
btree_search_leaf(node_header *node, btree_key *search_key)
{
  unsigned char *p= node->data;
  unsigned char *end= p + node->used_length;

  for (;;)
  {
    if (p >= end)
      return NULL;

    btree_key *key= search_key->from_memory(&p);
    int comparison= search_key->compare(key);

    if (comparison == 0)
      return key;

    delete key;

    if (comparison < 0)
      return NULL;
  }
}

/* Search for key from root, returning NULL if not found. */
btree_key *
btree_search(node_header *node, btree_key *search_key)
{
  if (node->flags & LEAF_NODE)
    return btree_search_leaf(node, search_key);

  unsigned char *p= node->data;
  unsigned char *end= p + node->used_length;

  node_header *child;
  for (;;)
  {
    memcpy(&child, p, sizeof(child));
    p+= sizeof(child);

    if (p >= end)
      return btree_search(child, search_key);

    btree_key *key= search_key->from_memory(&p);
    int comparison= search_key->compare(key);
    delete key;

    if (comparison <= 0)
      return btree_search(child, search_key);
  }
}
struct btree_insert_return {
  /* Only if status==SPLIT, the high half node of the split. */
  node_header *new_node;
  /* Only if status==SPLIT, the leftmost key of new node, must be delete()ed. */
  btree_key *new_leftmost_key;

  enum {
    DUPLICATE,
    NO_SPLIT,
    SPLIT
  } status;
};

/*
  Return:
    NULL - duplicate key found.
    node - returns passed-in node pointer to signal insert without split.
    otherwise - indicates split, returning new node with larger values.

    Nope, this does not work. We need to also return the smallest key in the
    new node.
*/
static struct btree_insert_return
btree_insert_leaf(node_header *node, btree_key *new_key)
{
  unsigned char *p= node->data;
  unsigned char *end_point= p + node->used_length;
  unsigned char *middle_point=
    node->data + (pagesize - sizeof(node_header) + 1)/2;
  unsigned char *split_point= NULL;
  unsigned char *pre_split_point= node->data;
  unsigned char *new_p;
  struct btree_insert_return ret;

  for (;;)
  {
    if (p >= end_point)
      break;

    new_p= p;
    btree_key *key= new_key->from_memory(&new_p);
    int comparison= new_key->compare(key);
    delete key;

    if (split_point == NULL)
    {
      if (new_p >= middle_point)
        split_point= p;
      else
        pre_split_point= p;
    }

    if (comparison == 0)
    {
      ret.status= btree_insert_return::DUPLICATE;
      return ret;
    }
    else if (comparison < 0)
    {
      break;
    }
    p= new_p;
  }
  unsigned char *insert_point= p;

  uint32_t key_size= new_key->size();
  if (sizeof(node_header) + node->used_length + key_size <= pagesize)
  {
    /* Key fits, no need to split. */
    if (insert_point < end_point)
      memmove(insert_point + key_size, insert_point, end_point - insert_point);
    new_key->to_memory(insert_point);
    node->used_length+= key_size;

    ret.status= btree_insert_return::NO_SPLIT;
    return ret;
  }
  else
  {
    /* Need to split the node. */
    node_header *new_node= (node_header *)malloc(pagesize);
    new_node->flags= LEAF_NODE;

    /* Find the point to split at, if not found already. */
    if (split_point == NULL)
    {
      /*
        At this point, new_p can not be uninitialised, as that would imply we
        are splitting an empty leaf node.
      */
      for(;;)
      {
        p= new_p;
        /*
          We check for end_point here just for style; however we are still
          missing proper checks against too big keys (must be able to fit at
          least two into every leaf node).
        */
        if (p >= end_point || (new_p= new_key->skip_memory(p)) >= middle_point)
        {
          split_point= p;
          break;
        }
        else
          pre_split_point= p;
      }
    }

    uint32_t new_key_size= new_key->size();
    uint32_t copy_length;

    if (insert_point <= split_point)
    {
      /* New key goes into first half. */

      if (split_point < end_point) {
        copy_length= end_point - split_point;
        memcpy(new_node->data, split_point, copy_length);
        new_node->used_length= copy_length;
      } else {
        new_node->used_length= 0;
      }

      if (insert_point < split_point)
      {
        copy_length= split_point - insert_point;
        memmove(insert_point + new_key_size, insert_point, copy_length);
      }

      new_key->to_memory(insert_point);
      node->used_length= (split_point - node->data) + new_key_size;
    }
    else
    {
      /* New key goes into second half. */
      node->used_length= split_point - node->data;

      unsigned char *p2= new_node->data;

      copy_length= insert_point - split_point;
      memcpy(p2, split_point, copy_length);
      p2 += copy_length;
      new_key->to_memory(p2);
      p2 += new_key_size;
      if (insert_point < end_point)
      {
        copy_length= end_point - insert_point;
        memcpy(p2, insert_point, copy_length);
        p2 += copy_length;
      }

      new_node->used_length= p2 - new_node->data;

    }
    ret.status= btree_insert_return::SPLIT;
    ret.new_node= new_node;
    ret.new_leftmost_key= new_key->from_memory(&pre_split_point);
    return ret;
  }
}

static btree_insert_return
btree_insert_recurse(node_header *node, btree_key *new_key)
{
  struct btree_insert_return ret;

  if (node->flags & LEAF_NODE)
    return btree_insert_leaf(node, new_key);

  /* Search for the child into which the key should be inserted. */
  unsigned char *p= node->data;
  unsigned char *end_point= p + node->used_length;
  unsigned char *middle_point=
    node->data + (pagesize - sizeof(node_header) + 1)/2;
  unsigned char *split_point= NULL;
  unsigned char *pre_split_point= node->data + sizeof(node_header *);

  node_header *child;
  unsigned char *new_p;
  for (;;)
  {
    memcpy(&child, p, sizeof(child));
    p+= sizeof(child);

    if (p >= end_point)
      break;

    new_p= p;
    btree_key *key= new_key->from_memory(&new_p);
    int comparison= new_key->compare(key);
    delete key;

    if (split_point == NULL)
    {
      if (new_p + sizeof(node_header *) >= middle_point)
        split_point= p;
      else
        pre_split_point= p;
    }

    if (comparison == 0)
    {
      ret.status = btree_insert_return::DUPLICATE;
      return ret;
    }
    else if (comparison < 0)
    {
      break;
    }
    p= new_p;
  }
//ToDo: Hm, is pre_split_point even needed here? I think not.
  struct btree_insert_return res= btree_insert_recurse(child, new_key);
  if (res.status != btree_insert_return::SPLIT)
    return res;

  /* The child node was split. Add the new child, splitting if needed. */
  unsigned char *insert_point= p;
  uint32_t key_size= res.new_leftmost_key->size();

  uint32_t move_distance= key_size + sizeof(node_header *);
  uint32_t needed_size=
    sizeof(node_header) + (end_point - node->data) + move_distance;
  if (needed_size <= pagesize)
  {
    /* No split needed. */
    if (insert_point < end_point)
    {
      uint32_t move_size= end_point - insert_point;
      memmove(insert_point + move_distance, insert_point, move_size);
    }
    res.new_leftmost_key->to_memory(insert_point);
    delete res.new_leftmost_key;
    memcpy(insert_point + key_size, &(res.new_node), sizeof(node_header *));
    node->used_length+= move_distance;

    res.status= btree_insert_return::NO_SPLIT;
    return res;
  }
  else
  {
    /* Split needed. */

    /* Find the point to split at, if not already found. */
    if (split_point == NULL) {
      for (;;) {
        /*
          new_p must be initialized, as otherwise we would be splitting an
          empty node.
        */
        new_p+= sizeof(node_header *);
        split_point= new_p;
        if (new_p > end_point)
          break;
        new_p= new_key->skip_memory(new_p);
        if (new_p >= middle_point)
          break;
      }
    }

    /*
      We now have to put the new leftmost key (followed by the new child node)
      at the insert_point, and then split the node at the split_point, passing
      the key at the split point up as the new leftmost key.

      As a special case, if insert_point==split_point, the leftmost key
      obtained from the child is not inserted into this node, but passed up as
      the leftmost key to the parent.
    */

    node->used_length= split_point - node->data;
    node_header *new_node= (node_header *)malloc(pagesize);
    new_node->flags= 0;                         // Not leaf node
    btree_key *new_leftmost_key;

    if (insert_point < split_point)
    {
      /* New leftmost key and new child goes in first half. */
      unsigned char *tmp= split_point;
      new_leftmost_key= new_key->from_memory(&tmp);
      uint32_t new_key_size= new_leftmost_key->size();
      uint32_t copy_length= end_point - (split_point + new_key_size);
      memcpy(new_node->data, split_point + new_key_size, copy_length);
      uint32_t move_distance= key_size + sizeof(node_header *);
      uint32_t move_length= split_point - insert_point;
      memmove(insert_point + move_distance, insert_point, move_length);
      res.new_leftmost_key->to_memory(insert_point);
      delete res.new_leftmost_key;
      memcpy(insert_point + key_size, &(res.new_node), sizeof(node_header *));
      node->used_length+= move_distance;
      new_node->used_length= copy_length;
    }
    else if (insert_point == split_point)
    {
      /*
        New leftmost key goes up to parent, new child goes at the start of
        second half.
      */
      unsigned char *p2= new_node->data;
      memcpy(p2, &(res.new_node), sizeof(node_header *));
      new_node->used_length= sizeof(node_header *);
      if (split_point < end_point)
      {
        uint32_t copy_length= end_point - split_point;
        memcpy(p2 + sizeof(node_header *), split_point, copy_length);
        new_node->used_length+= copy_length;
      }
      new_leftmost_key= res.new_leftmost_key;
    }
    else /* insert_point > split_point */
    {
      /* New leftmost key and new child goes in second half. */
      unsigned char *tmp= split_point;
      new_leftmost_key= new_key->from_memory(&tmp);
      uint32_t new_key_size= new_leftmost_key->size();
      uint32_t copy_length1= insert_point - (split_point + new_key_size);
      unsigned char *p2= new_node->data;
      memcpy(p2, split_point + new_key_size, copy_length1);
      p2+= copy_length1;
      res.new_leftmost_key->to_memory(p2);
      delete res.new_leftmost_key;
      p2+= key_size;
      memcpy(p2, &(res.new_node), sizeof(node_header *));
      p2+= sizeof(node_header *);
      if (insert_point < end_point)
      {
        uint32_t copy_length2= end_point - insert_point;
        memcpy(p2, insert_point, copy_length2);
        p2+= copy_length2;
      }
      new_node->used_length= p2 - new_node->data;
    }

    btree_insert_return ret;
    ret.new_node= new_node;
    ret.new_leftmost_key= new_leftmost_key;
    ret.status= btree_insert_return::SPLIT;
    return ret;
  }
}

/*
  Insert a key in root, returning false if ok and true if duplicate.
  Note that this may modify the root pointer (in case of root split).
*/
bool
btree_insert(node_header **root, btree_key *new_key)
{
  struct btree_insert_return res= btree_insert_recurse(*root, new_key);

  if (res.status == btree_insert_return::DUPLICATE)
    return true;

  if (res.status == btree_insert_return::SPLIT)
  {
    node_header *new_root= (node_header *)malloc(pagesize);
    uint32_t key_size= res.new_leftmost_key->size();
    new_root->used_length= key_size + 2*sizeof(node_header *);
    new_root->flags= 0;                      // Not leaf node
    unsigned char *p= new_root->data;
    memcpy(p, root, sizeof(node_header *));
    p+= sizeof(node_header *);
    res.new_leftmost_key->to_memory(p);
    p+= key_size;
    memcpy(p, &(res.new_node), sizeof(node_header *));
    delete res.new_leftmost_key;
    *root= new_root;
  }

  return false;                                 // No duplicate error
}

/*
  Traverse a BTree in sorted order, invoking callback on each key.
  The dummy key is used to understand the type of keys stored.
*/
void
btree_traverse(node_header *node, btree_key *dummy_key,
               void (*callback)(void *, btree_key *), void *user_data)
{
  unsigned char *p= node->data;
  unsigned char *end_point=  p + node->used_length;

  if (node->flags & LEAF_NODE)
  {
    while (p < end_point)
    {
      btree_key *key= dummy_key->from_memory(&p);
      (*callback)(user_data, key);
      delete key;
    }
  }
  else
  {
    for (;;)
    {
      node_header *child;
      memcpy(&child, p, sizeof(node_header *));
      p+= sizeof(node_header *);
      btree_traverse(child, dummy_key, callback, user_data);
      if (p >= end_point)
        break;
      p= dummy_key->skip_memory(p);
    }
  }
}

#ifdef TEST_BTREE
#include <stdio.h>

struct my_callback_data { uint64_t a,b; };

static void
my_traverse_callback(void *arg, btree_key *generic_key)
{
  my_key *key= (my_key *)generic_key;
  my_callback_data *data= (my_callback_data *)arg;
  if (!((data->a==key->k1 && data->b+1==key->k2) || (data->a+1==key->k1)))
  {
    printf("DIFF old[%" PRIu64 "d %" PRIu64 "d] new[%" PRIu64 "d %" PRIu64 "d]\n", data->a, data->b, key->k1, key->k2);
  }
  data->a= key->k1;
  data->b= key->k2;
}

int
main(int argc, char *argv[])
{
  uint64_t N= 10;
  if (argc > 1)
    N= strtoul(argv[1], NULL, 10);

  node_header *root= btree_get_empty();

  for (uint64_t i= 0; i < N; i++)
  {
    for (uint64_t j= 0; j < N; j++)
    {
      /* (j,i) to insert in non-sorted order. */
      my_key key(j, i);
      bool err= btree_insert(&root, &key);
      if (err)
        fprintf(stderr, "Got duplicate error inserting (%d, %d)\n", (int)j, (int)i);
    }
  }

  my_key dummy(0,0);
  my_callback_data data;
  data.a= (uint64_t)(int64_t)-1;
  data.b= 0;
  btree_traverse(root, &dummy, my_traverse_callback, &data);

  for (uint64_t i= 0; i < N; i++)
  {
    for (uint64_t j= 0; j < N; j++)
    {
      my_key search_key(i, j);
      btree_key *generic_key= btree_search(root, &search_key);
      if (generic_key == NULL)
      {
        printf("NOT FOUND! i=%d j=%d\n", (int)i, (int)j);
      }
      else
      {
        my_key *found_key= (my_key *)generic_key;
        if (i != found_key->k1 || j != found_key->k2)
          printf("WRONG KEY! search(%d, %d)  found(%d, %d)\n", (int)search_key.k1, (int)search_key.k2, (int)found_key->k1, (int)found_key->k2);
        delete generic_key;
      }
    }
  }

  btree_free(root, &dummy);

  return 0;
}
#endif
