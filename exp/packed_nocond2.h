/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Try for a version without conditional branches.

  Appears to be slower. I think this is due to doing _two_ loads per getbits(),
  whereas the conditional version can do << 1 load per getbits(). This overhead
  probably outweights any branch mispredictions.
*/

#include <stdint.h>
typedef unsigned int uint;

struct pack_ptr_nocond2 {
  uint64_t *base;
  uint64_t v1,v2;
//  uint i;
  uint64_t i;
//  uint bitpos;
  uint64_t bitpos;

  pack_ptr_nocond2(uint64_t *base);
  uint64_t getbits(uint64_t numbits);
  void putbits(uint numbits, uint64_t data);

  uint64_t unpack1();
  void pack1(uint64_t v);
  uint64_t unpack2();
  void pack2(uint64_t v);
  uint64_t unpack3();
  void pack3(uint64_t v);
  uint64_t unpack4();
  void pack4(uint64_t v);
};

inline
pack_ptr_nocond2::pack_ptr_nocond2(uint64_t *base)
{
  this->base= base;
  v1= base[0];
  v2= base[1];
  i= 2;
  bitpos= 0;
}

/*
  This is a version of getbits that does not have any conditional jumps.

  Note that it may read one additional 64-bit word after the last word
  containing data. The data read is not used in any way, but it must be
  addressable (ie. the extra read must not cause a segfault).
*/
inline uint64_t
pack_ptr_nocond2::getbits(uint64_t numbits)
{
//  uint b= bitpos;
  uint64_t b= bitpos;
//  uint new_b= b + numbits;
  uint64_t new_b= b + numbits;
//  uint64_t mask_undef_64bit_shift= (b == 0 ? 0 : ~(uint64_t)0);
  uint64_t mask_undef_64bit_shift= ((int64_t)__builtin_expect(!b, 0) - 1);
//  uint64_t mask_undef_64bit_shift= (int64_t)((((int64_t)b-1) & ~(uint64_t)b) >> 63) - 1;
  uint64_t r= (v1 >> b) | ( (v2 << (64 - b)) & mask_undef_64bit_shift);
//  uint delta= new_b;
  uint64_t delta= new_b;
/*
  delta>>= 6;
  if (delta) v1= v2;
  if (delta) v2= base[i];
*/
  asm ( "shrq $6, %[delta]\n\t"
        "cmovneq %[v2], %[v1]\n\t"
        "cmovneq (%[base], %[i], 8), %[v2]"
        : [delta] "=r" (delta)
        , [v1] "=r" (v1)
        , [v2] "=r" (v2)
        : "[delta]" (delta)
        , "[v1]" (v1)
        , "[v2]" (v2)
        , [base] "r" (base)
        , [i] "r" ((uint64_t)i)
        , "m" (base[i])
        : "cc"
        );
  i+= delta;
  bitpos = new_b & 63;
  return r & (~(uint64_t)0 >> (64 - numbits)); 
}

inline uint64_t
pack_ptr_nocond2::unpack1()
{
  uint n= getbits(3);
  return getbits(n*9+1);
}

inline uint64_t
pack_ptr_nocond2::unpack2()
{
  uint n= getbits(2);
  return getbits(n*20+4);
}
