/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdio.h>

#include "packed_mask.h"

uint64_t read1(uint64_t *base, uint count)
{
  uint64_t r= 0;
  pack_ptr_mask p(base);

  while (count--)
    r+= p.unpack1();

  return r;
}

uint64_t read2(uint64_t *base, uint count)
{
  uint64_t r= 0;
  pack_ptr_mask p(base);

  while (count--)
    r+= p.unpack2();

  return r;
}

int main(int argc, char *argv[])
{
  uint64_t word= 0xfedcba9876543210UL;
  pack_ptr_mask p(&word);
  printf("%lx\n", (unsigned long)p.getbits(3));
  printf("%lx\n", (unsigned long)p.getbits(7));
  return 0;
}
