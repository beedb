/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>


/*
  Version of packed_mask.h that uses uint64_t throughout, in an attempt to
  avoid sign extension overhead.
*/

/*
  ToDo: Need big-endian version of this, that will store from high bit down,
  so that the trailing bytes of any final partial word will be at the end.
*/

struct pack_ptr_unsigned {
  /*
    Base pointer.
    Should be 64-bit aligned, and must be readable as 64-bit words, even
    if trailing bits are undefined (ie. for a 9-byte buffer we will read
    16 bytes, though we will only actually look at the first 9).
  */
  uint64_t *b;
  /* Current word, equal to b[i-1]. */
  uint64_t v;
  /* Index of next word. */
  uint64_t i;
  /* Bit position in current word. */
  uint64_t p;

  /* Methods, all inline. */
  uint64_t used() { return i; }

  pack_ptr_unsigned(uint64_t *base);
  /* Read the next NUMBITS bits, updating pointer. */
  uint64_t getbits(uint64_t numbits);
  /*
    Put value DATA of size NUMBITS into current position.

    Assumes that DATA fits in a word of size NUMBITS, so we don't have to
    mask it ourselves.

    ToDo: We can have a more efficient version of this that assumes the data
    before the write is zero.

    Then there is no need to mask off the destination, and we don't have to
    write into the destination array except when moving to the next word (in
    this case we need a flush() method to be called at the end).

    We might want to have another struct for this, as we will need another
    constructor to initialise v=0, and we won't mix well with reading anyway.
  */
  void putbits(uint64_t numbits, uint64_t data);
  /* Read/store 64-bit value packed with 3 length bits. */
  uint64_t unpack1();
  void pack1(uint64_t v);
  uint64_t unpack2();
  void pack2(uint64_t v);
  uint64_t unpack3();
  void pack3(uint64_t v);
  uint64_t unpack4();
  void pack4(uint64_t v);
};

inline
pack_ptr_unsigned::pack_ptr_unsigned(uint64_t *base)
{
  b= base;
  v= b[0];
  i= 1;
  p= 0;
}

inline uint64_t
pack_ptr_unsigned::getbits(uint64_t numbits)
{
  uint64_t r= v >> p;
  p+= numbits;
  if (unlikely(p >= 64))
  {
    /*
      ToDo: Hm, seems we might be reading one extra word off the end here.

      May be worthwhile, but needs to be taken into account.
    */
    v= b[i++];
    p= p - 64;
    /*
      Shift by 64 bit is undefined in C.
      We make the conditional on numbits==64 so that for compile-time known
      value of numbits the compiler can optimize the conditional away.
      For numbits < 64 and p = 0, the redundant |= (which is masked away
      immediately below) is cheaper than an extra conditional.
    */
    if (likely(!(numbits == 64 && p == 0)))
      r|= v << (numbits - p);
  }
  r&= (~(uint64_t)0 >> (64 - numbits));
//printf("getbits(%u) -> %lu\n", numbits, (unsigned long)r);
  return r; 
}

inline void
pack_ptr_unsigned::putbits(uint64_t numbits, uint64_t data)
{
//printf("putbits(%u, %lu)\n", numbits, (unsigned long)data);
  uint64_t mask= (~(uint64_t)0 >> (64 - numbits));
  v= (v & ~(mask << p)) | (data << p);
  b[i-1]= v;
  p+= numbits;
  if (unlikely(p >= 64))
  {
    v= b[i];
    p= p - 64;
    if (likely(!(numbits == 64 && p == 0)))
    {
      v= (v & (~(uint64_t)0 << p)) | (data >> (numbits - p));
      b[i]= v;
    }
    i++;
  }
}

inline uint64_t
pack_ptr_unsigned::unpack1()
{
  uint64_t n= getbits(3);
  return getbits(n*9+1);
}

inline void
pack_ptr_unsigned::pack1(uint64_t v)
{
  /*
    Compute the 3-bit header counter C, where we store C*9+1 bits.
    We have C = ((#bits - 1) + 8) / 9   [rounding down].
    And #bits = 64 - #leading zeroes, and 64 - 1 + 8 == 71.
    We use |1 to be sure to get at least a size of one bit (__builtin_clzl is
    undefined for argument of zero).
  */
  uint64_t bits= (71 - COUNT_LEADING_ZEROS_64(v | 1))/9;
  putbits(3, bits);
  putbits(bits*9+1, v);
}

inline uint64_t
pack_ptr_unsigned::unpack2()
{
  uint64_t n= getbits(2);
  return getbits(n*20+4);
}

inline void
pack_ptr_unsigned::pack2(uint64_t v)
{
  uint64_t bits= (79 - COUNT_LEADING_ZEROS_64(v | 1))/20;
  putbits(2, bits);
  putbits(bits*20+4, v);
}

/*
  BE: 0xxx      LE:    xxx0
      10<11x>       <11x>01
      110<20x>     <20x>011
      111<64x>     <64x>111
*/

/* ToDo: Only one copy of this table! */
static struct {
  uint64_t bits;
  uint64_t or_value;
#ifndef WORDS_BIGENDIAN
  uint64_t lshift;
#endif
} unpack3_table_unsigned[8]= {
#ifdef WORDS_BIGENDIAN
  /* 0 */ { 1, 0 },
  /* 1 */ { 1, 2 },
  /* 2 */ { 1, 4 },
  /* 3 */ { 1, 6 },
  /* 4 */ { 10, 0 },
  /* 5 */ { 10, 1024 },
  /* 6 */ { 20, 0 },
  /* 7 */ { 64, 0}
#else
  /* 0 */ {  1, 0, 2 },
  /* 1 */ { 10, 0, 1 },
  /* 2 */ {  1, 1, 2 },
  /* 3 */ { 20, 0, 0 },
  /* 4 */ {  1, 2, 2 },
  /* 5 */ { 10, 1, 1 },
  /* 6 */ {  1, 3, 2 },
  /* 7 */ { 64, 0, 0 }
#endif
  };

inline uint64_t
pack_ptr_unsigned::unpack3()
{
  uint64_t prefix= getbits(3);
  return ( getbits(unpack3_table_unsigned[prefix].bits)
#ifndef WORDS_BIGENDIAN
           << unpack3_table_unsigned[prefix].lshift
#endif
         ) | unpack3_table_unsigned[prefix].or_value;
}

static struct { uint64_t tag_bits; uint64_t data_bits; uint64_t tag_value; }
pack3_table_unsigned[64]= {
#ifdef WORDS_BIGENDIAN
  /* 64 */ { 3, 64, 7 },
  /* 63 */ { 3, 64, 7 },
  /* 62 */ { 3, 64, 7 },
  /* 61 */ { 3, 64, 7 },
  /* 60 */ { 3, 64, 7 },
  /* 59 */ { 3, 64, 7 },
  /* 58 */ { 3, 64, 7 },
  /* 57 */ { 3, 64, 7 },
  /* 56 */ { 3, 64, 7 },
  /* 55 */ { 3, 64, 7 },
  /* 54 */ { 3, 64, 7 },
  /* 53 */ { 3, 64, 7 },
  /* 52 */ { 3, 64, 7 },
  /* 51 */ { 3, 64, 7 },
  /* 50 */ { 3, 64, 7 },
  /* 49 */ { 3, 64, 7 },
  /* 48 */ { 3, 64, 7 },
  /* 47 */ { 3, 64, 7 },
  /* 46 */ { 3, 64, 7 },
  /* 45 */ { 3, 64, 7 },
  /* 44 */ { 3, 64, 7 },
  /* 43 */ { 3, 64, 7 },
  /* 42 */ { 3, 64, 7 },
  /* 41 */ { 3, 64, 7 },
  /* 40 */ { 3, 64, 7 },
  /* 39 */ { 3, 64, 7 },
  /* 38 */ { 3, 64, 7 },
  /* 37 */ { 3, 64, 7 },
  /* 36 */ { 3, 64, 7 },
  /* 35 */ { 3, 64, 7 },
  /* 34 */ { 3, 64, 7 },
  /* 33 */ { 3, 64, 7 },
  /* 32 */ { 3, 64, 7 },
  /* 31 */ { 3, 64, 7 },
  /* 30 */ { 3, 64, 7 },
  /* 29 */ { 3, 64, 7 },
  /* 28 */ { 3, 64, 7 },
  /* 27 */ { 3, 64, 7 },
  /* 26 */ { 3, 64, 7 },
  /* 25 */ { 3, 64, 7 },
  /* 24 */ { 3, 64, 7 },
  /* 23 */ { 3, 64, 7 },
  /* 22 */ { 3, 64, 7 },
  /* 21 */ { 3, 64, 7 },
  /* 20 */ { 3, 20, 6 },
  /* 19 */ { 3, 20, 6 },
  /* 18 */ { 3, 20, 6 },
  /* 17 */ { 3, 20, 6 },
  /* 16 */ { 3, 20, 6 },
  /* 15 */ { 3, 20, 6 },
  /* 14 */ { 3, 20, 6 },
  /* 13 */ { 3, 20, 6 },
  /* 12 */ { 3, 20, 6 },
  /* 11 */ { 2, 11, 2 },
  /* 10 */ { 2, 11, 2 },
  /*  9 */ { 2, 11, 2 },
  /*  8 */ { 2, 11, 2 },
  /*  7 */ { 2, 11, 2 },
  /*  6 */ { 2, 11, 2 },
  /*  5 */ { 2, 11, 2 },
  /*  4 */ { 2, 11, 2 },
  /*  3 */ { 1,  3, 0 },
  /*  2 */ { 1,  3, 0 },
  /*  1 */ { 1,  3, 0 },
#else
  /* 64 */ { 3, 64, 7 },
  /* 63 */ { 3, 64, 7 },
  /* 62 */ { 3, 64, 7 },
  /* 61 */ { 3, 64, 7 },
  /* 60 */ { 3, 64, 7 },
  /* 59 */ { 3, 64, 7 },
  /* 58 */ { 3, 64, 7 },
  /* 57 */ { 3, 64, 7 },
  /* 56 */ { 3, 64, 7 },
  /* 55 */ { 3, 64, 7 },
  /* 54 */ { 3, 64, 7 },
  /* 53 */ { 3, 64, 7 },
  /* 52 */ { 3, 64, 7 },
  /* 51 */ { 3, 64, 7 },
  /* 50 */ { 3, 64, 7 },
  /* 49 */ { 3, 64, 7 },
  /* 48 */ { 3, 64, 7 },
  /* 47 */ { 3, 64, 7 },
  /* 46 */ { 3, 64, 7 },
  /* 45 */ { 3, 64, 7 },
  /* 44 */ { 3, 64, 7 },
  /* 43 */ { 3, 64, 7 },
  /* 42 */ { 3, 64, 7 },
  /* 41 */ { 3, 64, 7 },
  /* 40 */ { 3, 64, 7 },
  /* 39 */ { 3, 64, 7 },
  /* 38 */ { 3, 64, 7 },
  /* 37 */ { 3, 64, 7 },
  /* 36 */ { 3, 64, 7 },
  /* 35 */ { 3, 64, 7 },
  /* 34 */ { 3, 64, 7 },
  /* 33 */ { 3, 64, 7 },
  /* 32 */ { 3, 64, 7 },
  /* 31 */ { 3, 64, 7 },
  /* 30 */ { 3, 64, 7 },
  /* 29 */ { 3, 64, 7 },
  /* 28 */ { 3, 64, 7 },
  /* 27 */ { 3, 64, 7 },
  /* 26 */ { 3, 64, 7 },
  /* 25 */ { 3, 64, 7 },
  /* 24 */ { 3, 64, 7 },
  /* 23 */ { 3, 64, 7 },
  /* 22 */ { 3, 64, 7 },
  /* 21 */ { 3, 64, 7 },
  /* 20 */ { 3, 20, 3 },
  /* 19 */ { 3, 20, 3 },
  /* 18 */ { 3, 20, 3 },
  /* 17 */ { 3, 20, 3 },
  /* 16 */ { 3, 20, 3 },
  /* 15 */ { 3, 20, 3 },
  /* 14 */ { 3, 20, 3 },
  /* 13 */ { 3, 20, 3 },
  /* 12 */ { 3, 20, 3 },
  /* 11 */ { 2, 11, 1 },
  /* 10 */ { 2, 11, 1 },
  /*  9 */ { 2, 11, 1 },
  /*  8 */ { 2, 11, 1 },
  /*  7 */ { 2, 11, 1 },
  /*  6 */ { 2, 11, 1 },
  /*  5 */ { 2, 11, 1 },
  /*  4 */ { 2, 11, 1 },
  /*  3 */ { 1,  3, 0 },
  /*  2 */ { 1,  3, 0 },
  /*  1 */ { 1,  3, 0 },
#endif
  };

inline void
pack_ptr_unsigned::pack3(uint64_t v)
{
  uint64_t bits= COUNT_LEADING_ZEROS_64(v | 1);
  putbits(pack3_table_unsigned[bits].tag_bits, pack3_table_unsigned[bits].tag_value);
  putbits(pack3_table_unsigned[bits].data_bits, v);
}

/*
  BE: 00xx      LE:    xx00
      01<8x>         <8x>01
      10<16x>       <16x>10
      110<24x>     <24x>011
      111<64x>     <64x>111
*/

/* ToDo: Only one copy of this table! */
static struct {
  uint64_t bits;
  uint64_t or_value;
#ifndef WORDS_BIGENDIAN
  uint64_t lshift;
#endif
} unpack4_table_unsigned[8]= {
#ifdef WORDS_BIGENDIAN
  /* 0 */ { 1, 0 },
  /* 1 */ { 1, 2 },
  /* 2 */ { 7, 0 },
  /* 3 */ { 7, 128 },
  /* 4 */ { 15, 0 },
  /* 5 */ { 15, 32768 },
  /* 6 */ { 24, 0 },
  /* 7 */ { 64, 0}
#else
  /* 0 */ {  1, 0, 1 },
  /* 1 */ {  7, 0, 1 },
  /* 2 */ { 15, 0, 1 },
  /* 3 */ { 24, 0, 0 },
  /* 4 */ {  1, 1, 1 },
  /* 5 */ {  7, 1, 1 },
  /* 6 */ { 15, 1, 1 },
  /* 7 */ { 64, 0, 0 }
#endif
  };

inline uint64_t
pack_ptr_unsigned::unpack4()
{
  uint64_t prefix= getbits(3);
  return ( getbits(unpack4_table_unsigned[prefix].bits)
#ifndef WORDS_BIGENDIAN
           << unpack4_table_unsigned[prefix].lshift
#endif
         ) | unpack4_table_unsigned[prefix].or_value;
}

static struct { uint64_t tag_bits; uint64_t data_bits; uint64_t tag_value; }
pack4_table_unsigned[64]= {
#ifdef WORDS_BIGENDIAN
  /* 64 */ { 3, 64, 7 },
  /* 63 */ { 3, 64, 7 },
  /* 62 */ { 3, 64, 7 },
  /* 61 */ { 3, 64, 7 },
  /* 60 */ { 3, 64, 7 },
  /* 59 */ { 3, 64, 7 },
  /* 58 */ { 3, 64, 7 },
  /* 57 */ { 3, 64, 7 },
  /* 56 */ { 3, 64, 7 },
  /* 55 */ { 3, 64, 7 },
  /* 54 */ { 3, 64, 7 },
  /* 53 */ { 3, 64, 7 },
  /* 52 */ { 3, 64, 7 },
  /* 51 */ { 3, 64, 7 },
  /* 50 */ { 3, 64, 7 },
  /* 49 */ { 3, 64, 7 },
  /* 48 */ { 3, 64, 7 },
  /* 47 */ { 3, 64, 7 },
  /* 46 */ { 3, 64, 7 },
  /* 45 */ { 3, 64, 7 },
  /* 44 */ { 3, 64, 7 },
  /* 43 */ { 3, 64, 7 },
  /* 42 */ { 3, 64, 7 },
  /* 41 */ { 3, 64, 7 },
  /* 40 */ { 3, 64, 7 },
  /* 39 */ { 3, 64, 7 },
  /* 38 */ { 3, 64, 7 },
  /* 37 */ { 3, 64, 7 },
  /* 36 */ { 3, 64, 7 },
  /* 35 */ { 3, 64, 7 },
  /* 34 */ { 3, 64, 7 },
  /* 33 */ { 3, 64, 7 },
  /* 32 */ { 3, 64, 7 },
  /* 31 */ { 3, 64, 7 },
  /* 30 */ { 3, 64, 7 },
  /* 29 */ { 3, 64, 7 },
  /* 28 */ { 3, 64, 7 },
  /* 27 */ { 3, 64, 7 },
  /* 26 */ { 3, 64, 7 },
  /* 25 */ { 3, 64, 7 },
  /* 24 */ { 3, 24, 6 },
  /* 23 */ { 3, 24, 6 },
  /* 22 */ { 3, 24, 6 },
  /* 21 */ { 3, 24, 6 },
  /* 20 */ { 3, 24, 6 },
  /* 19 */ { 3, 24, 6 },
  /* 18 */ { 3, 24, 6 },
  /* 17 */ { 3, 24, 6 },
  /* 16 */ { 2, 16, 2 },
  /* 15 */ { 2, 16, 2 },
  /* 14 */ { 2, 16, 2 },
  /* 13 */ { 2, 16, 2 },
  /* 12 */ { 2, 16, 2 },
  /* 11 */ { 2, 16, 2 },
  /* 10 */ { 2, 16, 2 },
  /*  9 */ { 2, 16, 2 },
  /*  8 */ { 2,  8, 1 },
  /*  7 */ { 2,  8, 1 },
  /*  6 */ { 2,  8, 1 },
  /*  5 */ { 2,  8, 1 },
  /*  4 */ { 2,  8, 1 },
  /*  3 */ { 2,  8, 1 },
  /*  2 */ { 2,  2, 0 },
  /*  1 */ { 2,  2, 0 },
#else
  /* 64 */ { 3, 64, 7 },
  /* 63 */ { 3, 64, 7 },
  /* 62 */ { 3, 64, 7 },
  /* 61 */ { 3, 64, 7 },
  /* 60 */ { 3, 64, 7 },
  /* 59 */ { 3, 64, 7 },
  /* 58 */ { 3, 64, 7 },
  /* 57 */ { 3, 64, 7 },
  /* 56 */ { 3, 64, 7 },
  /* 55 */ { 3, 64, 7 },
  /* 54 */ { 3, 64, 7 },
  /* 53 */ { 3, 64, 7 },
  /* 52 */ { 3, 64, 7 },
  /* 51 */ { 3, 64, 7 },
  /* 50 */ { 3, 64, 7 },
  /* 49 */ { 3, 64, 7 },
  /* 48 */ { 3, 64, 7 },
  /* 47 */ { 3, 64, 7 },
  /* 46 */ { 3, 64, 7 },
  /* 45 */ { 3, 64, 7 },
  /* 44 */ { 3, 64, 7 },
  /* 43 */ { 3, 64, 7 },
  /* 42 */ { 3, 64, 7 },
  /* 41 */ { 3, 64, 7 },
  /* 40 */ { 3, 64, 7 },
  /* 39 */ { 3, 64, 7 },
  /* 38 */ { 3, 64, 7 },
  /* 37 */ { 3, 64, 7 },
  /* 36 */ { 3, 64, 7 },
  /* 35 */ { 3, 64, 7 },
  /* 34 */ { 3, 64, 7 },
  /* 33 */ { 3, 64, 7 },
  /* 32 */ { 3, 64, 7 },
  /* 31 */ { 3, 64, 7 },
  /* 30 */ { 3, 64, 7 },
  /* 29 */ { 3, 64, 7 },
  /* 28 */ { 3, 64, 7 },
  /* 27 */ { 3, 64, 7 },
  /* 26 */ { 3, 64, 7 },
  /* 25 */ { 3, 64, 7 },
  /* 24 */ { 3, 24, 3 },
  /* 23 */ { 3, 24, 3 },
  /* 22 */ { 3, 24, 3 },
  /* 21 */ { 3, 24, 3 },
  /* 20 */ { 3, 24, 3 },
  /* 19 */ { 3, 24, 3 },
  /* 18 */ { 3, 24, 3 },
  /* 17 */ { 3, 24, 3 },
  /* 16 */ { 2, 16, 2 },
  /* 15 */ { 2, 16, 2 },
  /* 14 */ { 2, 16, 2 },
  /* 13 */ { 2, 16, 2 },
  /* 12 */ { 2, 16, 2 },
  /* 11 */ { 2, 16, 2 },
  /* 10 */ { 2, 16, 2 },
  /*  9 */ { 2, 16, 2 },
  /*  8 */ { 2,  8, 1 },
  /*  7 */ { 2,  8, 1 },
  /*  6 */ { 2,  8, 1 },
  /*  5 */ { 2,  8, 1 },
  /*  4 */ { 2,  8, 1 },
  /*  3 */ { 2,  8, 1 },
  /*  2 */ { 2,  2, 0 },
  /*  1 */ { 2,  2, 0 },
#endif
  };

inline void
pack_ptr_unsigned::pack4(uint64_t v)
{
  uint64_t bits= COUNT_LEADING_ZEROS_64(v | 1);
  putbits(pack4_table_unsigned[bits].tag_bits, pack4_table_unsigned[bits].tag_value);
  putbits(pack4_table_unsigned[bits].data_bits, v);
}

/*
  32-bit: 2+n*10

  00 xx
  01 xxxxxxxxxxxx
  10 xxxxxxxxxxxxxxxxxxxxxx
  11 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

*/
