/*
  ToDo ideas:

   - A version with non-temporal writes. But wait until we see that it is
     actually useful. It seems in many cases we will want the destination
     bytes in cache, eg. due to fractional cache line writes or maybe we will
     soon do further processing such as write to disk or socket.
*/



/*
  This inline copy is more efficient for small sizes when both source,
  destination, and copy size are 64-bit aligned.

  To emphasise the alignment requirement, src_offset, dst_offset, and
  wordcount are all specified in units of 64 bits.
*/
static inline void
inline_bitcopy_aligned(uint64_t *dst, uint64_t *src, uint64_t wordcount)
{
  uint64_t *src_end= src + wordcount;

  while (src < src_end)
  {
    *dst++= *src++;
  }
}

void bitcopy_aligned(uint64_t *dst, uint64_t *src, uint64_t wordcount);

/*
  This inline copy is more efficient for small sizes when the source is known
  to be 64-bit aligned.

  Note that while the dst_pos counts bits, the src_offset counts 64-bit words
  (so dst_pos is in units of bits, while src_offset is in units of 64 bits),
  emphasising the aligned requirement for src.

  The unit of numbits is just bits like dst_pos, so there is no requirement
  for the size to be 64-bit aligned.

  Note that in case of fractional last destination word, the content of the
  remaining bits after the copy proper is undefined.
*/
static inline void
inline_bitcopy_aligned_src(uint64_t *dst_base, uint64_t dst_pos, uint64_t *src,
                           uint64_t numbits)
{
  uint64_t dst_word_pos= dst_pos & 0x3f;
  uint64_t *dst= dst_base + (dst_pos >> 6);
  uint64_t count= (numbits + 0x3f) >> 6;
  if (dst_word_pos == 0)
  {
    inline_bitcopy_aligned(dst, src, count);
  }
  else
  {
    uint64_t bits_in_first= 64 - dst_word_pos;
    uint64_t d= *dst & (~(uint64_t)0 >> bits_in_first);
    uint64_t *src_end= src + count;
    while(src < src_end)
    {
      uint64_t v= *src++;
      *dst++= d | (v << dst_word_pos);
      d= v >> bits_in_first;
    }

    uint64_t final_bits= numbits & 0x3f;
    /*
      Check if we need to fill one extra word of the destination.

      This is necessary if (final_bits > bits_in_first). But there is another
      special case when final_bits==0, since then we _always_ need to fill one
      extra word (as dst_word_pos > 0).

      This conditional combines both of these cases into one.
    */
    if (final_bits + dst_word_pos > ((final_bits + 0x3f) & (uint64_t)0x40))
      *dst= d;
  }
}

void bitcopy_aligned_src(uint64_t *dst_base, uint64_t dst_pos, uint64_t *src,
                         uint64_t numbits);

/*
  This inline copy is more efficient for small sizes when the destination is
  known to be 64-bit aligned.

  The numbits parameter is in units of bits, so there is no requirement for
  the copy size to be 64-bit aligned.

  If the size is not a multiple of 64 bits, the contents of the final word
  after the bits copied are not defined.
*/
static inline void
inline_bitcopy_aligned_dst(uint64_t *dst, uint64_t *src_base, uint64_t src_pos,
                           uint64_t numbits)
{
  uint64_t src_word_pos= src_pos & 0x3f;
  uint64_t *src= src_base + (src_pos >> 6);
  if (src_word_pos == 0)
  {
    inline_bitcopy_aligned(dst, src, (numbits + 0x3f) >> 6);
  }
  else
  {
    uint64_t v= *src++;
    uint64_t d;
    uint64_t count= (numbits + src_word_pos - 1) >> 6;
    uint64_t *src_end= src + count;
    uint64_t bits_in_first= 64 - src_word_pos;
    while (src < src_end)
    {
      d= v >> src_word_pos;
      v= *src++;
      *dst++= d | (v << bits_in_first);
    }

    if (count < ((numbits + 0x3f) >> 6))
      *dst= v >> src_word_pos;
  }
}

void bitcopy_aligned_dst(uint64_t *dst, uint64_t *src_base, uint64_t src_pos,
                         uint64_t numbits);

void bitcopy(uint64_t *dst_base, uint64_t dst_pos, uint64_t *src_base,
             uint64_t src_pos, uint64_t numbits);

void bitcopy2(uint64_t *dst_base, uint64_t dst_pos,
              uint64_t *src_base, uint64_t src_pos,
              uint64_t numbits);
void bitcopy3(uint64_t *dst_base, uint64_t dst_pos,
              uint64_t *src_base, uint64_t src_pos,
              uint64_t numbits);
