/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

#include "packed_mask.h"
#include "packed_nocond.h"
#include "packed_nocond2.h"
#include "packed_del.h"
#include "packed.h"
#include "packed_unsigned.h"

void
fill_expanded(uint64_t *p, uint64_t count)
{
  FILE *f= fopen("numbers.txt", "r");
  if (!f)
  {
    perror("cannot open number file");
    exit(1);
  }

  while (count > 0)
  {
    unsigned long v;
    int res= fscanf(f, "%lu", &v);
    if (res == EOF)
    {
      rewind(f);
      continue;
    }
    else if (res != 1)
    {
      fprintf(stderr, "Error reading number from file.\n");
      exit(1);
    }
    *p++= v;
    count--;
  }
}

/* Assumes sufficient buffer. */
void
fill_packed(pack_overwriteptr &p, uint64_t count)
{
  FILE *f= fopen("numbers.txt", "r");
  if (!f)
  {
    perror("cannot open number file");
    exit(1);
  }

  while (count > 0)
  {
    unsigned long v;
    int res= fscanf(f, "%lu", &v);
    if (res == EOF)
    {
      rewind(f);
      continue;
    }
    else if (res != 1)
    {
      fprintf(stderr, "Error reading number from file.\n");
      exit(1);
    }
    p.pack1(v);
    count--;
  }
  p.flush();
}

/* Assumes sufficient buffer. */
void
fill_packed_del(pack_ptr_del &p, uint64_t count)
{
  FILE *f= fopen("numbers.txt", "r");
  if (!f)
  {
    perror("cannot open number file");
    exit(1);
  }

  while (count > 0)
  {
    unsigned long v;
    int res= fscanf(f, "%lu", &v);
    if (res == EOF)
    {
      rewind(f);
      continue;
    }
    else if (res != 1)
    {
      fprintf(stderr, "Error reading number from file.\n");
      exit(1);
    }
    p.pack1(v);
    count--;
  }
}

static double
gettime(void)
{
  struct timeval tv;
  if (gettimeofday(&tv, NULL))
  {
    perror("gettimeofday()");
    exit(1);
  }
  return (double)tv.tv_sec + (double)tv.tv_usec*1e-6;
}


NOINLINE uint64_t
test_signed_encode(int64_t i) {
  return pack_ptr::signed_encode(i);
}

NOINLINE int64_t
test_signed_decode(uint64_t u) {
  return pack_ptr::signed_decode(u);
}


NOINLINE static uint64_t
run_sum(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j=  loops; j > 0; j--)
  {
    for (uint i= 0; i < count; i++)
    {
      sum+= base[i];
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_del(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j=  loops; j > 0; j--)
  {
    pack_ptr_del p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.getbits(33);
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_getbits_nocond2(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr_nocond2 p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.getbits(33);
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_packed_nocond2(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr_nocond2 p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.unpack1();
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_getbits(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.getbits(33);
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_getbits_mask(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr_mask p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.getbits(33);
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_getbits_unsigned(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr_unsigned p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.getbits(33);
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_packed(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.unpack1();
    }
  }
  return sum;
}

NOINLINE static uint64_t
run_sum_packed5(uint count, uint loops, uint64_t *base)
{
  uint64_t sum= 0;
  for (uint j= loops; j > 0; j--)
  {
    pack_ptr p(base);
    for (uint i= count; i > 0; i--)
    {
      sum+= p.unpack5();
    }
  }
  return sum;
}

NOINLINE static void
run_copy(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    for (uint i= 0; i < count; i++)
    {
      /*
        Use only a small bit of source that fits in L1 cache.
        We want to time the writing, not the reading.
      */
      dst[i]= src[i&511];
    }
  }
}

#include <xmmintrin.h>
void _mm_stream_pi(__m64 *p, __m64 a);

NOINLINE static void
run_copy_non_temporal(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  if ((count & 7) != 0) {
    fprintf(stderr, "run_copy_non_temporal(): Count must be divisible by 8 "
            "for this test, but %u isn't.\n", count);
    return;
  }
  count /= 8;

  for (uint j= loops; j > 0; j--)
  {
    __m64 *mm_dst= (__m64 *)dst;
    for (uint i= 0; i < count; i++)
    {
      uint src_i= i & 511;
      _mm_stream_pi(mm_dst, (__m64)(src[src_i]));
      _mm_stream_pi(mm_dst+1, (__m64)(src[src_i+1]));
      _mm_stream_pi(mm_dst+2, (__m64)(src[src_i+2]));
      _mm_stream_pi(mm_dst+3, (__m64)(src[src_i+3]));
      _mm_stream_pi(mm_dst+4, (__m64)(src[src_i+4]));
      _mm_stream_pi(mm_dst+5, (__m64)(src[src_i+5]));
      _mm_stream_pi(mm_dst+6, (__m64)(src[src_i+6]));
      _mm_stream_pi(mm_dst+7, (__m64)(src[src_i+7]));
      mm_dst += 8;
    }
  }
}

NOINLINE static void
run_copy_non_temporal_no_unroll(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  if ((count & 7) != 0) {
    fprintf(stderr, "run_copy_non_temporal(): Count must be divisible by 8 "
            "for this test, but %u isn't.\n", count);
    return;
  }
  count /= 8;

  for (uint j= loops; j > 0; j--)
  {
    __m64 *mm_dst= (__m64 *)dst;
    for (uint i= 0; i < count; i++)
    {
      uint src_i= i & 511;
      for (uint j= 0; j < 8; j++) {
        _mm_stream_pi(mm_dst+j, (__m64)(src[src_i+j]));
      }
      mm_dst += 8;
    }
  }
}

NOINLINE static void
run_pack1(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    pack_overwriteptr p(dst);
    for (uint i= 0; i < count; i++)
    {
      p.pack1(src[i&511]);
    }
    p.flush();
  }
}

NOINLINE static void
run_pack2(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    pack_overwriteptr p(dst);
    for (uint i= 0; i < count; i++)
    {
      p.pack2(src[i&511]);
    }
    p.flush();
  }
}

NOINLINE static void
run_pack3(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    pack_overwriteptr p(dst);
    for (uint i= 0; i < count; i++)
    {
      p.pack3(src[i&511]);
    }
    p.flush();
  }
}

NOINLINE static void
run_pack4(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    pack_overwriteptr p(dst);
    for (uint i= 0; i < count; i++)
    {
      p.pack4(src[i&511]);
    }
    p.flush();
  }
}

NOINLINE static void
run_pack5(uint count, uint loops, uint64_t *dst, uint64_t *src)
{
  for (uint j= loops; j > 0; j--)
  {
    pack_overwriteptr p(dst);
    for (uint i= 0; i < count; i++)
    {
      p.pack5(src[i&511]);
    }
    p.flush();
  }
}

int
main(int argc, char *argv[])
{
  uint count = 2000000;  // Main memory
  uint loops= 1000;
//  uint count = 100000;     // L2
//  uint loops = 20000;
//  uint count = 2000;     // L1
//  uint loops= 1000000;

//  uint count = 1;        // debugging

  uint64_t *b1 = (uint64_t *)calloc(count, sizeof(uint64_t));
  uint64_t *b2 = (uint64_t *)calloc(count*2, sizeof(uint64_t));
  uint64_t *b3 = (uint64_t *)calloc(count*2, sizeof(uint64_t));
  if(!b1 || !b2 || !b3)
  {
    fprintf(stderr, "No mem.\n");
    exit(1);
  }

  fill_expanded(b1, count);
  printf("Expanded: %ld bytes\n", (long)(count*sizeof(*b1)));

  /* Fill in some prior data to check correctness on non-zero destination. */
  memcpy(b2, b1, count*sizeof(*b1));
  pack_overwriteptr p(b2);
  fill_packed(p, count);
  printf("Packed: %ld bytes\n", (long)(p.used_words()*sizeof(*b2)));

  /* Check correctness of pack5() and unpack5(). */
  memcpy(b3, b1, count*sizeof(*b1));
  pack_overwriteptr p5a(b3);
  for (uint i= 0; i < count; i++) {
    p5a.pack5(b1[i]);
  }
  p5a.flush();
  printf("Packed: %ld bytes\n", (long)(p5a.used_words()*sizeof(*b2)));
  pack_ptr p5b(b3);
  for (uint i= 0; i < count; i++) {
    uint64_t v= p5b.unpack5();
    if (v != b1[i]) {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack5=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  uint64_t sum;

  /* Do in a loop to time packing. */
  double start1= gettime();
  run_copy(count, loops, b3, b1);
  double elapsed1= gettime() - start1;
  printf("copy: time=%g\n", elapsed1);
  start1= gettime();
  run_copy_non_temporal(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("copy_non_temporal: time=%g\n", elapsed1);
  start1= gettime();
  run_copy_non_temporal_no_unroll(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("copy_non_temporal_no_unroll: time=%g\n", elapsed1);
  start1= gettime();
  run_pack1(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("pack1: time=%g\n", elapsed1);
  start1= gettime();
  run_pack2(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("pack2: time=%g\n", elapsed1);
  start1= gettime();
  run_pack3(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("pack3: time=%g\n", elapsed1);
  start1= gettime();
  run_pack4(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("pack4: time=%g\n", elapsed1);
  start1= gettime();
  run_pack5(count, loops, b3, b1);
  elapsed1= gettime() - start1;
  printf("pack5: time=%g\n", elapsed1);
  start1= gettime();
  sum= run_sum_packed5(count, loops, b3);
  elapsed1= gettime() - start1;
  printf("exp: unpack5() sum=%lu time=%g\n", (unsigned long)sum, elapsed1);

  pack_ptr_del p0(b3);
  fill_packed_del(p0, count);
  printf("Packed_del: %ld bytes\n", (long)(p0.used()*sizeof(*b3)));

  /* Check correctness. */
  pack_ptr_mask p2(b2);
  for (uint i= 0; i < count; i++)
  {
    uint64_t v= p2.unpack1();
    if (v != b1[i])
    {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  pack_ptr_nocond p2a(b2);
  for (uint i= 0; i < count; i++)
  {
    uint64_t v= p2a.unpack1();
    if (v != b1[i])
    {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack_nocond=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  pack_ptr_nocond2 p2b(b2);
  for (uint i= 0; i < count; i++)
  {
    uint64_t v= p2b.unpack1();
    if (v != b1[i])
    {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack_nocond2=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  pack_ptr p2c(b2);
  for (uint i= 0; i < count; i++)
  {
    uint64_t v= p2c.unpack1();
    if (v != b1[i])
    {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  pack_ptr_del p1(b3);
  for (uint i= 0; i < count; i++)
  {
    uint64_t v= p1.unpack1();
    if (v != b1[i])
    {
      printf("DIFF! i=%u exp=%lu (0x%lx) pack_del=%lu (0x%lx)\n",
             i, b1[i], b1[i], v, v);
      exit(1);
    }
  }

  /* Time looping over data. */
  sum= 0;
  double start= gettime();
  double elapsed;
  sum= run_sum(count, loops, b1);
  elapsed= gettime() - start;
  printf("exp: sum=%lu time=%g\n", (unsigned long)sum, elapsed);

#if 0
  uint32_t sum32= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    for (uint i= 0; i < count; i++)
    {
      sum32+= ((uint32_t *)b1)[i];
    }
  }
  elapsed= gettime() - start;
  printf("exp32: sum=%lu time=%g\n", (unsigned long)sum32, elapsed);

  sum= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    pack_ptr_mask p3(b2);
    for (uint i= 0; i < count; i++)
    {
      sum+= p3.unpack1();
    }
  }
  elapsed= gettime() - start;
  printf("pack_mask: sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  sum= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    pack_ptr_nocond p4(b1);
    for (uint i= 0; i < count; i++)
    {
      sum+= p4.getbits(33);
    }
  }
  elapsed= gettime() - start;
  printf("getbits_nocond(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  sum= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    pack_ptr_nocond p4(b2);
    for (uint i= 0; i < count; i++)
    {
      sum+= p4.unpack1();
    }
  }
  elapsed= gettime() - start;
  printf("pack_nocond: sum=%lu time=%g\n", (unsigned long)sum, elapsed);
#endif

  start= gettime();
  sum= run_sum_getbits(count, loops, b1);
  elapsed= gettime() - start;
  printf("getbits(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  start= gettime();
  sum= run_sum_getbits_mask(count, loops, b1);
  elapsed= gettime() - start;
  printf("getbits_mask(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  start= gettime();
  sum= run_sum_getbits_unsigned(count, loops, b1);
  elapsed= gettime() - start;
  printf("getbits_unsigned(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  start= gettime();
  sum= run_sum_packed(count, loops, b2);
  elapsed= gettime() - start;
  printf("pack: sum=%lu time=%g\n", (unsigned long)sum, elapsed);

#if 0
  start= gettime();
  sum= run_sum_getbits_nocond2(count, loops, b2);
  elapsed= gettime() - start;
  printf("getbits_nocond2(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  start= gettime();
  sum= run_sum_packed_nocond2(count, loops, b2);
  elapsed= gettime() - start;
  printf("pack_nocond2: sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  sum= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    pack_ptr_del p6(b1);
    for (uint i= 0; i < count; i++)
    {
      sum+= p6.getbits_le(33);
    }
  }
  elapsed= gettime() - start;
  printf("getbits_le_del(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  start= gettime();
  sum= run_sum_del(count, loops, b1);
  elapsed= gettime() - start;
  printf("getbits_del(33): sum=%lu time=%g\n", (unsigned long)sum, elapsed);

  sum= 0;
  start= gettime();
  for (uint j= 0; j < loops; j++)
  {
    pack_ptr_del p8(b3);
    for (uint i= 0; i < count; i++)
    {
      sum+= p8.unpack1();
    }
  }
  elapsed= gettime() - start;
  printf("pack_del: sum=%lu time=%g\n", (unsigned long)sum, elapsed);
#endif

  return 0;
}
