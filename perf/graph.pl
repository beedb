#! /usr/bin/perl

use strict;
use warnings;

use POSIX();
use GD::Graph::bars;
use GD::Graph::hbars;
use DBI;




# Find a good max value that is a nice round number and not too far
# from given max data value.
sub calc_good_max {
  my ($value)= @_;
  $value= -$value if $value < 0;
  $value= 1 if $value == 0;
  my $base= 10**POSIX::floor(log($value)/log(10));
  for (1, 2.5, 5, 10)
  {
    my $candidate= $_*$base;
    return $candidate if $candidate >= $value;
  }
  # Shouldn't really be possible to get here...
  return $value;
}


my ($host, $user, $pass)= @ARGV;

my $dbh= DBI->connect("dbi:mysql:database=beedb;host=$host", $user, $pass,
                      { RaiseError => 1, PrintError => 0 });

my $sql= <<SQL;
SELECT instance_idx, counter_idx, variant, iterations, workunits, counter_name, counter_value
  FROM perf_test_run
 INNER JOIN perf_counts ON (perf_test_run.id = perf_counts.test_run_id)
 WHERE perf_test_run.suite_run_id = 17
   AND test_major = 'bitcopy'
   AND test_minor = 'bitcopy'
   AND param1 = 'bits=17'
   AND param2 = 'offs s=17 d=42'
 ORDER BY counter_name, instance_idx, counter_idx, variant
SQL
my $sth= $dbh->prepare($sql);
$sth->execute();
my $data= [];
my $idx_x;
my $idx_y= 0;
my ($last_instance, $last_counter);
my $variant_names= [];
my $max_y= undef;
my ($instance_idx, $counter_idx, $variant, $iterations, $workunits, $counter_name, $counter_value);

while (my $r= $sth->fetchrow_arrayref())
{
  ($instance_idx, $counter_idx, $variant, $iterations, $workunits, $counter_name, $counter_value)= @$r;
  if ($instance_idx == 0 && $counter_idx == 0)
  {
    push @$variant_names, $variant;
  }

  if ((!defined($last_instance) || $last_instance != $instance_idx) ||
      (!defined($last_counter) || $last_counter != $counter_idx))
  {
    if (!defined($last_instance))
    {
      $idx_y= 0;
    }
    else
    {
      $idx_y++;
    }
    $idx_x= 1;
  }

  $iterations= 1 unless $iterations;
  $workunits= 1 unless $workunits;
  my $value= $counter_value / $iterations / $workunits;
  $max_y= $value unless defined($max_y) && $max_y >= $value;
  $data->[$idx_x][$idx_y]= $value;
  $data->[0][$idx_y]= $counter_name if $idx_x == 1;
  $last_instance= $instance_idx;
  $last_counter= $counter_idx;
  $idx_x++;
}

use Data::Dumper;
# print Dumper($data);
# print Dumper($variant_names);

my $graph= GD::Graph::hbars->new(1400, 800);
$graph->set( x_label => "Counter name",
             y_label => "Counts per iteration(I) per workunit(U)",
             title => "bitcopy/bitcopy [bits=17; offs s=17 d=42] I=$iterations U=$workunits",
             y_max_value => calc_good_max($max_y),
             bargroup_spacing => 4,
    );
$graph->set_legend(@$variant_names);
$graph->plot($data);

binmode STDOUT;
my $ext= $graph->export_format();
print $graph->gd->$ext();
