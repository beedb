#! /usr/bin/perl

use strict;
use warnings;

use DBI;

use Sys::Hostname();

sub parse_output {
  my ($fh, $callback)= @_;

  my $current;
  my $state= undef;
  my ($run_index, $counter_index, $fixed_count);

  while(<$fh>)
  {
    print;
    if (/^T: (.*?)( \{(.*?)\})?( \[(.*?)(;.*?)?\])? I=([0-9]+)( U=([0-9]+))?$/) {
      $callback->($current) if defined($current);
      $current= { MINOR => $1,
                  ITERATIONS => $7,
                  LIST => []
      };
      $current->{VARIANT}= $3 if defined($3);
      if (defined($5)) {
        $current->{PARAM1}= $5;
        $current->{PARAM2}= substr($6, 1) if defined($6);
      }
      $current->{WORKUNITS}= $9 if defined($9);
      $run_index= undef;
      $counter_index= 0;
    }
    elsif (/^  Seconds: ([0-9]+\.[0-9]+)(( [-+][0-9]+\.[0-9]+)*)$/)
    {
      $current->{ELAPSED}= [$1];
      push @{$current->{ELAPSED}}, $1 + $_ for (split(" ", $2));
    }
    elsif (/^  (F: |V[0-9]:|   ) ([^ ]+)\s+([0-9]+)((\s+[-+][0-9]+)*)$/)
    {
      my ($hdr, $name, $count, $deltas)= ($1, $2, $3, $4);
      if ($hdr eq 'F: ' || ($hdr eq '   ' && !defined($run_index)))
      {
        my $idx= 0;
        $current->{LIST}[$idx][$counter_index]= {NAME => $name,
                                                 COUNT => $count };
        for (split(" ", $deltas))
        {
          $idx++;
          $current->{LIST}[$idx][$counter_index]= {NAME => $name,
                                                   COUNT => $count + $_ };
        }
        $counter_index++;
        $fixed_count= $counter_index;
      }
      else
      {
        if ($hdr =~ /^V([0-9]):$/)
        {
          $run_index= $1 - 1;
          $counter_index= $fixed_count;
        }

        $current->{LIST}[$run_index][$counter_index]= { NAME => $name,
                                                        COUNT => $count };
        $counter_index++;
      }
    }
  }

  $callback->($current) if defined($current);
}

sub insert_in_db {
  my ($dbh, $e, $suite_id, $major)= @_;

  $dbh->begin_work();

  my $sql= <<SQL;
INSERT INTO perf_test_run(suite_run_id, test_major, test_minor, param1, param2,
            variant, iterations, workunits)
     VALUES (?, ?, ?, ?, ?, ?, ?, ?)
SQL
  $dbh->do($sql, undef, $suite_id, $major, $e->{MINOR}, $e->{PARAM1}, $e->{PARAM2},
           $e->{VARIANT}, $e->{ITERATIONS}, $e->{WORKUNITS});
#  my $test_run_id= $dbh->{mysql_inserid};
my $test_run_id= $dbh->selectall_arrayref("SELECT LAST_INSERT_ID()")->[0][0];

  $sql= "INSERT INTO perf_wallclock(test_run_id, instance_idx, secs) VALUES";
  my $sep= "";
  my @values= ();
  for (my $idx= 0; $idx < @{$e->{ELAPSED}}; $idx++)
  {
    $sql.= $sep ." (?, ?, ?)";
    $sep= ",";
    push @values, $test_run_id, $idx, $e->{ELAPSED}[$idx];
  }
  $dbh->do($sql, undef, @values);

  $sql= "INSERT INTO perf_counts(test_run_id, instance_idx, counter_idx, counter_name, counter_value) VALUES";
  $sep= "";
  @values= ();
  for (my $run_idx= 0; $run_idx < @{$e->{LIST}}; $run_idx++)
  {
    for (my $counter_idx= 0; $counter_idx < @{$e->{LIST}[$run_idx]}; $counter_idx++)
    {
      $sql.= $sep ." (?, ?, ?, ?, ?)";
      $sep= ",";
      push @values, $test_run_id, $run_idx, $counter_idx,
           $e->{LIST}[$run_idx][$counter_idx]{NAME},
           $e->{LIST}[$run_idx][$counter_idx]{COUNT};
    }
  }
  $dbh->do($sql, undef, @values);

  $dbh->commit();
}

sub get_host_id {
  my ($dbh)= @_;

  my $hostname= Sys::Hostname::hostname();

  my $os= qx{uname -o};
  chomp($os);
  my $kernel= qx{uname -r};
  chomp($kernel);
  my $arch= qx{uname -m};
  chomp($arch);
  my $cpu_name= "<unknown>";
  my $cpu_mhz= "<unknown>";
  if (open(FH, '<', '/proc/cpuinfo'))
  {
    while (<FH>)
    {
      $cpu_name= $1 if /^model name\s*	: (.*)$/;
      $cpu_mhz= $1 if /^cpu MHz\s*	: (.*)$/;
      # For multicore we will get multiple hits, but it doesn't really matter.
    }
    close FH;
  }

  my $sql= <<SQL;
SELECT id
  FROM perf_host
 WHERE hostname = ? AND os = ? AND kernel = ? AND arch = ? AND cpu_name = ? AND cpu_mhz = ?
SQL
  my $res= $dbh->selectall_arrayref($sql, undef,
                                    $hostname, $os, $kernel, $arch, $cpu_name, $cpu_mhz);
  return $res->[0][0] if ($res && @$res);

  # Not there already, so need to insert a new one.
  $sql= <<SQL;
INSERT INTO perf_host (hostname, os, kernel, arch, cpu_name, cpu_mhz)
VALUES (?, ?, ?, ?, ?, ?)
SQL
  $dbh->do($sql, undef, $hostname, $os, $kernel, $arch, $cpu_name, $cpu_mhz);
  #my $shost_id= $dbh->{mysql_inserid};
  my $host_id= $dbh->selectall_arrayref("SELECT LAST_INSERT_ID()")->[0][0];
  return $host_id;
}

sub get_git_rev {
  my $rev= qx{git-rev-list HEAD^..HEAD};
  if ($rev)
  {
    chomp($rev);

    # Check for local uncommitted modifications.
    system("git-diff", "--quiet");
    $rev= "* ". $rev if $?;

    return $rev;
  }
  else
  {
    return "<Unknown>";
  }
}

sub get_suite_run_id {
  my ($dbh)= @_;

  my $host_id= get_host_id($dbh);
  my $git_rev= get_git_rev();
  $dbh->do("INSERT INTO perf_suite_run(dt, code_rev, host_id) VALUES (NOW(), ?, ?)",
           undef, $git_rev, $host_id);
  #my $suite_run_id= $dbh->{mysql_inserid};
  my $suite_run_id= $dbh->selectall_arrayref("SELECT LAST_INSERT_ID()")->[0][0];
  return $suite_run_id;
}


die "Usage: $0 <DBHOST> <DBUSER> <DBPASS> [test ...]\n"
    unless @ARGV >= 3;
my ($host, $user, $pass, @requested_tests)= @ARGV;

my $dbh= DBI->connect("dbi:mysql:database=beedb;host=$host", $user, $pass,
                      { RaiseError => 1, PrintError => 0 });

my $test_list;

if (@requested_tests) {
  $test_list= [map($_ . (/\.t$/ ? '' : '.t'), @requested_tests)];
} else {
  $test_list= [glob('*.t')];
}

exit 0 unless @$test_list;

my $suite_run_id= get_suite_run_id($dbh);

for my $test (@$test_list)
{
  my $test_major= substr($test, 0, -2);

  open CHILD, "./$test|"
      or die "Failed to spawn child '$test': $!\n";
  print "Running test '$test'...\n";
  parse_output(\*CHILD, sub { insert_in_db($dbh, $_[0], $suite_run_id, 'bitcopy'); });
  close CHILD;
}

$dbh->disconnect();
