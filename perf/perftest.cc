/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  BeeDB is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  BeeDB is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with BeeDB.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Helper code for BeeDB performance regression testing.
*/

/*
  Need to put this early, as it does not work after some other system header
  might include inttypes.h
*/
#include "port/format_macros.h"

#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "beedb.h"
#include "perftest.h"

/*
  For now we are based on libpfm4/perf_event to do our stuff.

  We might want later to support other methods or at least fallback to a
  simple portable time measurement. But for now its important to get some
  results.

  Initially there are even Core 2 specific parts in the list of performance
  counters to use, though that should be easy to extend to other CPUs later.
*/


perftest::perftest()
{
  static bool perfmon_inited= false;
  if (!perfmon_inited)
  {
    int ret= pfm_initialize();
    if (ret != PFM_SUCCESS)
      fatal_error("Cannot initialize libperfmon: %s\n", pfm_strerror(ret));
    perfmon_inited= true;
  }

  memset(perf_counter_values, 0, sizeof(perf_counter_values));
  setup_perfmon();
}

perftest::~perftest()
{
  pfm_terminate();
}

void
perftest::run_test(test *t, uint64_t loops)
{
  printf("T: %s", t->text);
  if (t->variant != NULL)
    printf(" {%s}", t->variant);
  if (t->param1 != NULL)
  {
    printf(" [%s", t->param1);
    if (t->param2 != NULL)
      printf(";%s", t->param2);
    printf("]");
  }
  printf(" I=%" PRIu64, loops);
  if (t->workunits != 0)
    printf(" U=%" PRIu64, t->workunits);
  printf("\n");

  /* First run it without timing, to warm up caches etc. */
  t->prepare(-1);
  t->run(loops);

  /* Now run the test over all the performance counter runs. */
  for (int run= 0; run < num_runs; run++)
  {
    t->prepare(run);
    t->run(loops);
  }

  printf("  Seconds: %.4f", t->elapsed_time[0]);
  for (int run= 1; run < num_runs; run++)
    printf(" %+.4f", t->elapsed_time[run] - t->elapsed_time[0]);
  printf("\n");
  report_perfmon();
}

double
perftest::gettime(void)
{
  struct timeval tv;
  if (gettimeofday(&tv, NULL))
  {
    perror("gettimeofday()");
    exit(1);
  }
  return (double)tv.tv_sec + (double)tv.tv_usec*1e-6;
}

void
perftest::fatal_error(const char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  fatal_error(format, ap);
  va_end(ap);
}

void
perftest::fatal_error(const char *format, va_list ap)
{
  vfprintf(stderr, format, ap);
  exit(1);
}

void
perftest::prepare_perfmon_event(int idx, int run, const char *event_name)
{
  /*
    ToDo: This sets us to count only while in user mode (PFM_PLM3).
    We may want to make this configurable per-test or something.
  */
  int ret= pfm_get_perf_event_encoding(event_name, PFM_PLM3,
                                       &(perf_attr[run][idx]), NULL, NULL);
  if (ret != PFM_SUCCESS)
    fatal_error("Failed to prepare event '%s': %s\n",
                event_name, pfm_strerror(ret));
  perf_event_names[run][idx]= event_name;
}

static struct {
  const char *event0;
  const char *event1;
} core2_events[]= {
  { "RS_UOPS_DISPATCHED",             "UOPS_RETIRED:ANY" },
  { "INST_RETIRED:LOADS",             "INST_RETIRED:STORES" },
  { "BRANCH_INSTRUCTIONS_RETIRED",    "MISPREDICTED_BRANCH_RETIRED" },
  { "MEM_LOAD_RETIRED:L1D_LINE_MISS", "CYCLES_L1I_MEM_STALLED" },
  { "MEM_LOAD_RETIRED:L2_LINE_MISS",  "RS_UOPS_DISPATCHED_NONE" },
  { "MEM_LOAD_RETIRED:DTLB_MISS",     "L1D_PREFETCH:REQUESTS" }
};

void
perftest::setup_perfmon()
{
  memset(perf_attr, 0, sizeof(perf_attr));
  /* Compute performance counter config for all runs. */
  for (int run= 0; run < num_runs; run++)
  {
    prepare_perfmon_event(0, run, "UNHALTED_CORE_CYCLES");
    prepare_perfmon_event(1, run, "INSTRUCTIONS_RETIRED");
    prepare_perfmon_event(2, run, core2_events[run].event0);
    prepare_perfmon_event(3, run, core2_events[run].event1);
    for (unsigned i= 0; i < num_counters; i++)
    {
      /* Type, config, and exclude_* are set in prepare_perfmon_event(). */
      perf_attr[run][i].size= sizeof(perf_attr[run][i]);
      /* Could later add time enabled/running. */
      perf_attr[run][i].read_format= PERF_FORMAT_GROUP;
      perf_attr[run][i].disabled= (i == 0);
      perf_attr[run][i].pinned= (i == 0);
    }
  }
}

perftest::test::test(perftest *tester, const char *text, const char *variant,
                     const char *param1, const char *param2, uint64_t workunits)
  : text(text), variant(variant), param1(param1), param2(param2),
    workunits(workunits), tester(tester)
{
}

void
perftest::test::fatal_error(const char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  tester->fatal_error(format, ap);
  va_end(ap);
}

void
perftest::test::start()
{
  if (current_run < 0)
    return;

  start_time= gettime();
  tester->start_perfmon(current_run);
}

void
perftest::test::record_time(const char *text)
{
  if (current_run < 0)
    return;

  tester->record_perfmon(current_run);
  elapsed_time[current_run]= gettime() - start_time;
  tester->record_perfmon_not_time_critical(current_run);
}

void
perftest::start_perfmon(int run)
{
  if (skip_perfmon)
    return;

  for (unsigned i= 0; i < num_counters; i++)
  {
    perf_fds[i]= perf_event_open(&(perf_attr[run][i]), getpid(), -1,
                                 (i ? perf_fds[0] : -1), 0);
    if (perf_fds[i] < 0)
      fatal_error("perf_event_open(%d) failed: ret=%d errno=%d (%s)\n",
                  i, perf_fds[i], errno, strerror(errno));

    int ret= ioctl(perf_fds[i], PERF_EVENT_IOC_RESET, 0);
    if (ret)
      fatal_error("ioctl(%d, PERF_EVENT_IOC_RESET) error, ret=%d errno=%d\n",
                  i, ret, errno);
  }
  int ret= ioctl(perf_fds[0], PERF_EVENT_IOC_ENABLE, 0);
  if (ret)
    fatal_error("ioctl(0, PERF_EVENT_IOC_ENABLE) error, ret=%d errno=%d\n",
                ret, errno);
}

void
perftest::record_perfmon(int run)
{
  if (skip_perfmon)
    return;

  int ret= ioctl(perf_fds[0], PERF_EVENT_IOC_DISABLE, 0);
  if (ret)
    fatal_error("ioctl(0, PERF_EVENT_IOC_DISABLE, 0) failed: ret=%d errno=%d\n",
                ret, errno);
}

/*
  We split this out from record_perfmon(), as it is not time critical (once
  the counters are stopped, the read out can happen at any point after with no
  difference in result), so other time critical tasks (ie. stopping wallclock)
  can be done first.
*/
void
perftest::record_perfmon_not_time_critical(int run)
{
  uint64_t buf[num_counters+1];

  if (skip_perfmon)
    return;

  int actual= read(perf_fds[0], &(buf[0]), sizeof(buf));
  if (actual != sizeof(buf))
    fatal_error("Perf_event read(%d) failed: ret=%d errno=%d (%s)\n",
                perf_fds[0], actual, errno, strerror(errno));
  if (buf[0] != num_counters)
    fatal_error("Perf_event read() returned %"PRIu64", expected %u.\n",
                buf[0], num_counters);
  for (unsigned i= 0; i < num_counters; i++)
  {
    perf_counter_values[run][i]= buf[i+1];
    close(perf_fds[i]);
  }
}

static int
get_num_digits_64(uint64_t value)
{
  int digits= 1;
  uint64_t x= 10;
  while (value >= x)
  {
    digits++;
    if (x > UINT64_MAX/10)
      break;
    x*= 10;
  }
  return digits;
}

void
perftest::report_perfmon()
{
  const char *name;

  /* To get a nicely aligned output, we first get the lengths of everything. */
  int counter_name_max= 1;
  int counter_value_max= 1;
  int counter_delta_max[num_runs - 1];
  for (int run= 1; run < num_runs; run++)
    counter_delta_max[run - 1]= 0;

  for (int run= 0; run < num_runs; run++)
  {
    for (unsigned int i= 0; i < num_counters; i++)
    {
      name= perf_event_names[run][i];
      int len= strlen(name);
      if (len > counter_name_max)
        counter_name_max= len;
      /*
         For the fixed counters, which are the same in every run, we output
         the value only in the first run, and deltas for the rest.
      */
      if (run == 0 || i >= num_fixed_counters)
      {
        len= get_num_digits_64(perf_counter_values[run][i]);
        if (len > counter_value_max)
          counter_value_max= len;
      }
      else
      {
        uint64_t first_value= perf_counter_values[0][i];
        uint64_t this_value= perf_counter_values[run][i];
        uint64_t delta;
        if (first_value >= this_value)
          delta= first_value - this_value;
        else
          delta= this_value - first_value;
        len= get_num_digits_64(delta);
        if (len > counter_delta_max[run - 1])
          counter_delta_max[run - 1]= len;
      }
    }
  }

  for (unsigned int i= 0; i < num_fixed_counters; i++)
  {
    name= perf_event_names[0][i];
    uint64_t first_value= perf_counter_values[0][i];
    printf("  %s %-*s %*" PRIu64, (i == 0 ? "F: " : "   "),
           counter_name_max, name,
           counter_value_max, first_value);
    for (int run= 1; run < num_runs; run++)
    {
      int64_t delta;
      uint64_t value= perf_counter_values[run][i];
      if (value > first_value)
        delta= (int64_t)(value - first_value);
      else
        delta= -(int64_t)(first_value - value);
      printf(" %+*" PRIi64, 1 + counter_delta_max[run - 1], delta);
    }
    printf("\n");
  }

  for (int run= 0; run < num_runs; run++)
  {
    for (unsigned int i= num_fixed_counters; i < num_counters; i++)
    {
      name= perf_event_names[run][i];
      uint64_t value= perf_counter_values[run][i];
      if (i == num_fixed_counters)
        printf("  V%d: ", run + 1);
      else
        printf("      ");
      printf("%-*s %*" PRIu64 "\n",
             counter_name_max, name, counter_value_max, value);
    }
  }
}

perftest::test::~test()
{
}
