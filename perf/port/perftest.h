/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  System-dependent part of helper code for BeeDB performance regression
  testing.
*/

/*
  This is helpfull for forcing the value of an expression to be considered
  needed by the compiler.

  (Ie. GCC is quite good at completely removing even complex computations and
  loops where the result is not needed (dead code elimination), which can be
  annoying when doing performance testing.
*/

#ifdef __GNUC__

inline void
perftest::force_value(uint64_t value)
{
  asm volatile("nop" : : "r" (value));
}

#else
#error Missing implementation for non-GCC
#endif
