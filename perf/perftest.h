/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Helper code for BeeDB performance regression testing.
*/

#include <stdarg.h>

/* For now, we are libperfmon specific. */
#include <perfmon/pfmlib_perf_event.h>


class perftest {
 public:
  /* The number of times to run each test benchmark different measurements. */
  static const int num_runs= 6;

  /* Virtual class, performance tests are implemented as derived classes. */
  class test {
  public:
    /*
      Constructor for one test case.

      Test description parameters:

      text
          Overall name of the test. Eg. "btree insert", or "memcpy".

      variant
          Identifies different variants of the test identified by parameter
          text. Could be different implementations of the same algorithm
          compared against each other. Idea is it makes sense to directly
          compare performance of different variants against each other.

      param1
      param2
          These are arbitrary parameters that the performance could be graphed
          against. Idea is that the tests will run for different values of
          these parameters, with eg. graphs drawn with performance as a
          function of these parameters. Either or both can be left NULL,
          meaning there is no such parameter to graph against. Could be eg.
          buffer size, or number of columns in a row.

      workunits
          This is the amount of work done during one iteration of the loop
          being timed in the run() method. Idea is that workunits*iterations
          would be some kind of measure of total amount of work done.

      Note that it is explicitly permitted to pass pointers for text, variant,
      param1, and param2 that are initialised only after calling this
      constructor (but before calling run()).
     */
    test(perftest *tester, const char *text, const char *variant= NULL,
         const char *param1= NULL, const char *param2= NULL,
         uint64_t workunits= 0);
    virtual void run(uint64_t loops) = 0;
    virtual ~test() = 0;

    double gettime() { return tester->gettime(); }
    void force_value(uint64_t value) { tester->force_value(value); }
    void fatal_error(const char *format, ...) NORETURN PRINTF_LIKE(2, 3);

    void prepare(int run) { current_run= run; }

    const char *text;
    const char *variant;
    const char *param1;
    const char *param2;
    uint64_t workunits;
    double elapsed_time[num_runs];

  protected:
    /*
      Start the timer running.
      Call just before starting the computation to be tested.
    */
    void start();
    void record_time(const char *text);

    perftest *const tester;

  private:
    int current_run;
    double start_time;
  };

  /* Methods. */
  perftest();
  ~perftest();
  void run_test(test *t, uint64_t loops);
  void start_perfmon(int run);
  void record_perfmon(int run);
  void record_perfmon_not_time_critical(int run);
  void report_perfmon();

  static double gettime();
  static void force_value(uint64_t value);
  static void fatal_error(const char *format, ...) NORETURN PRINTF_LIKE(1, 2);
  static void fatal_error(const char *format, va_list ap) NORETURN PRINTF_LIKE(1, 0);

  /* perf_event specific stuff. */
  static const unsigned int num_fixed_counters= 2;
  static const unsigned int num_unfixed_counters= 2;
  static const unsigned int num_counters=
    num_fixed_counters + num_unfixed_counters;

 private:
  perf_event_attr perf_attr[num_runs][num_counters];
  const char *perf_event_names[num_runs][num_counters];
  uint64_t perf_counter_values[num_runs][num_counters];
  int perf_fds[num_counters];

  /*
    Quick&dirty way to disable all performance counter usage (to allow
    external profiling tools to be used.
  */
  static const bool skip_perfmon= false;

  void prepare_perfmon_event(int idx, int run, const char *event_name);
  void setup_perfmon();
};

#include "port/perftest.h"
