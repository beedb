/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Performance test for bitcopy().
*/

#include "port/format_macros.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "beedb.h"
#include "perftest.h"
#include "bitcopy.h"


template<void (copier)(uint64_t *, uint64_t, uint64_t *, uint64_t, uint64_t)>
class pf_bitcopy : public perftest::test {
public:
  pf_bitcopy(perftest *tester, const char *text,
             uint64_t src_pos, uint64_t dst_pos, uint64_t numbits) :
    test(tester, "bitcopy", text, param1_buf, param2_buf, numbits),
    src_pos(src_pos), dst_pos(dst_pos), numbits(numbits)
  {
    memset(param1_buf, 0, sizeof(param1_buf));
    snprintf(param1_buf, sizeof(param1_buf) - 1, "bits=%" PRIu64, numbits);
    memset(param2_buf, 0, sizeof(param2_buf));
    snprintf(param2_buf, sizeof(param2_buf) - 1,
             "offs s=%" PRIu64 " d=%" PRIu64, src_pos, dst_pos);
  }

  void run(uint64_t loops);
//  ~pf_bitcopy() { }

private:
  uint64_t src_pos;
  uint64_t dst_pos;
  uint64_t numbits;
  char param1_buf[100];
  char param2_buf[100];
};

template<void (copier)(uint64_t *, uint64_t, uint64_t *, uint64_t, uint64_t)>
void
pf_bitcopy<copier>::run(uint64_t loops)
{
  uint64_t src_words= (src_pos + numbits + 0x3f) >> 6;
  uint64_t dst_words= (dst_pos + numbits + 0x3f) >> 6;
  uint64_t *src= (uint64_t *)calloc(src_words, sizeof(uint64_t));
  uint64_t *dst= (uint64_t *)malloc(src_words * sizeof(uint64_t));
  if (!src || !dst)
    fatal_error("Out of memory");

  start();

  for (uint64_t i= loops; i > 0; i--)
  {
    copier(dst, dst_pos, src, src_pos, numbits);
  }

  record_time(text);

  /*
    Do something with buffer to discourage the compiler from optimising
    everything away.
  */
  uint64_t value= 0;
  for (uint64_t i= dst_pos; i < dst_pos + numbits; i++)
  {
    value^= dst[i >> 6] >> (i & 0x3f);
  }
  force_value(value);

  free(dst);
  free(src);
}

int
main(int argc, char *argv[])
{
  perftest tester;

  pf_bitcopy<bitcopy> t1(&tester, "bitcopy", 17, 42, 17);
  tester.run_test(&t1, 100000000);
  pf_bitcopy<bitcopy2> t2(&tester, "bitcopy2", 17, 42, 17);
  tester.run_test(&t2, 100000000);
  pf_bitcopy<bitcopy3> t3(&tester, "bitcopy3", 17, 42, 17);
  tester.run_test(&t3, 100000000);

  pf_bitcopy<bitcopy> t4(&tester, "bitcopy", 17, 42, 176);
  tester.run_test(&t4, 100000000);
  pf_bitcopy<bitcopy2> t5(&tester, "bitcopy2", 17, 42, 176);
  tester.run_test(&t5, 100000000);
  pf_bitcopy<bitcopy3> t6(&tester, "bitcopy3", 17, 42, 176);
  tester.run_test(&t6, 100000000);

  pf_bitcopy<bitcopy> t7(&tester, "bitcopy", 17, 42, 17600);
  tester.run_test(&t7, 1000000);
  pf_bitcopy<bitcopy2> t8(&tester, "bitcopy2", 17, 42, 17600);
  tester.run_test(&t8, 1000000);
  pf_bitcopy<bitcopy3> t9(&tester, "bitcopy3", 17, 42, 17600);
  tester.run_test(&t9, 1000000);

  pf_bitcopy<bitcopy> t10(&tester, "bitcopy", 17, 42, 1760000);
  tester.run_test(&t10, 10000);
  pf_bitcopy<bitcopy2> t11(&tester, "bitcopy2", 17, 42, 1760000);
  tester.run_test(&t11, 10000);
  pf_bitcopy<bitcopy3> t12(&tester, "bitcopy3", 17, 42, 1760000);
  tester.run_test(&t12, 10000);

  return 0;
}
