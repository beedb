/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Performance test for lffifo classes, experimenting with memory barrier
  performance.
*/

#include "port/format_macros.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <pthread.h>

#include "beedb.h"
#include "perftest.h"


/* Yeah, would be better with proper lffifo.h interface file. */
#include "../exp/lffifo.cc"


/*
  Run a performance test on some variant of a lock-free fifo.

  Use a template, as overhead of virtual function call etc. could potentially
  dwarf what we are measuring.

  Run a loop writing or reading a lffifo, with a (non-measured) thread
  handling the other end of the lffifo.
*/
template<class LF>
class pf_lffifo : public perftest::test {
public:
  /* Which end are we testing: writer or reader? */
  enum enum_tested_end
  {
    TEST_WRITER,
    TEST_READER
  };

  pf_lffifo(perftest *tester, const char *variant, enum_tested_end _what_tested,
            uint64_t fifo_size, uint64_t _batch_size) :
    test(tester,
         (_what_tested == TEST_WRITER ? "lffifo_writer" : "lffifo_reader"),
         variant, param1_buf, param2_buf, _batch_size),
    what_tested(_what_tested),
    batch_size(_batch_size),
    my_lffifo(fifo_size)
  {
    memset(param1_buf, 0, sizeof(param1_buf));
    snprintf(param1_buf, sizeof(param1_buf) - 1,
             "batch=%" PRIu64, batch_size);
    memset(param2_buf, 0, sizeof(param2_buf));
    snprintf(param2_buf, sizeof(param2_buf) - 1,
             "fifosize=%" PRIu64, fifo_size);
  }

  void run(uint64_t loops);
  void *other_end_thread_func();

private:
  enum_tested_end what_tested;
  uint64_t batch_size;
  char param1_buf[100];
  char param2_buf[100];
  uint64_t loops;
  LF my_lffifo;
};

template<class LF>
static void *
lffifo_other_end(void *arg)
{
  pf_lffifo<LF> *test= static_cast<pf_lffifo<LF> *>(arg);
  return test->other_end_thread_func();
}

template<class LF>
void
pf_lffifo<LF>::run(uint64_t loops)
{
  int err;
  pthread_t my_thread;

  this->loops= loops;

  err= pthread_create(&my_thread, NULL, lffifo_other_end<LF>, this);
  if (err)
    fatal_error("Error: could not create thread: %d\n", err);

  my_lffifo.reset_stats();

  start();

  uint64_t v= 1;
  uint64_t count= loops;
  uint64_t sum= 0;
  uint64_t flush_count= batch_size;
  while (count--)
  {
    my_lffifo.put(v);
    if (!(flush_count--))
    {
      my_lffifo.write_flush();
      flush_count= batch_size;
    }
    sum+= v;
    v++;
  }

  record_time(text);
  /* Make sure other end will get the last bits. */
  my_lffifo.write_flush();

  printf("Producer: cpu=%d sum=%" PRIu64 " spins=%" PRIu64 " buf=%p\n",
         sched_getcpu(), sum, my_lffifo.wr_spin_loops, my_lffifo.buffer);

  void *dummy_result;
  err= pthread_join(my_thread, &dummy_result);
  if (err)
    fprintf(stderr, "Warning: pthread_join() failed: %d\n", err);
}

template<class LF>
void *
pf_lffifo<LF>::other_end_thread_func()
{
  uint64_t count= loops;
  uint64_t sum= 0;

  while (count--)
  {
    uint64_t v= my_lffifo.get();
    sum+= v;
  }

  printf(" Consumer: cpu=%d sum=%" PRIu64 " spins=%" PRIu64 "\n",
         sched_getcpu(), sum, my_lffifo.rd_spin_loops);
  return NULL;
}

int
main(int argc, char *argv[])
{
  perftest tester;

  pf_lffifo<lffifo> t1(&tester, "lffifo", pf_lffifo<lffifo>::TEST_WRITER, 1024, 16);
  tester.run_test(&t1, 100000000);

  return 0;
}
