/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Some helper code for writing unit tests using TAP.
*/

class beedb_tester {
 public:
  beedb_tester(int n);
  void run(void (*test)(beedb_tester *));
  template<class t> void run(void (*test)(t, beedb_tester *), t arg);
  void ok();
  void ok(const char *format, ...);
  void nok();
  void nok(const char *format, ...);

 private:
  int total_count;
  int current_test;
};

/*
  This needs to be inline here, the joys of C++ templates

  (C++ needs the definition to be able to generate versions for each
  argument type, there is no automatic boxing in C++).
*/
template <class t>
void
beedb_tester::run(void (*test)(t, beedb_tester *), t arg)
{
  current_test++;
  (*test)(arg, this);
}


/*
  Simple way to include the file name and line number into a "not ok" message.

  Idea is to use as t->nok(NOK_IDENT "foobar not found (%d)", x);
*/
#define NOK_IDENT __FILE__ ":" MACRO_NUMBER_TO_STRING(__LINE__) " "
