/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Implementation of class beedb_tester.
*/

#include <stdio.h>
#include <stdarg.h>

#include "beedb.h"
#include "beedb_tester.h"

beedb_tester::beedb_tester(int n) :
  total_count(n),
  current_test(0)
{
  printf("%d..%d\n", 1, total_count);
}

void
beedb_tester::run(void (*test)(beedb_tester *))
{
  current_test++;
  (*test)(this);
}

void
beedb_tester::ok()
{
  ok("");
}

void
beedb_tester::ok(const char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  printf("ok %d ", current_test);
  vprintf(format, ap);
  printf("\n");
  va_end(ap);
}

void
beedb_tester::nok()
{
  nok("");
}

void
beedb_tester::nok(const char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  printf("not ok %d ", current_test);
  vprintf(format, ap);
  printf("\n");
  va_end(ap);
}
