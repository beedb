#! /usr/bin/perl

# Copyright 2009 Kristian Nielsen
#
# This file is part of BeeDB.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

# The Perl test framework runs test programs as Perl scripts. But some
# of our test programs need to be written in C++.
#
# This wrapper simply calls on to the C++ binary of the same name
# (without .pl).
#
# Later we can add any further Perl stuff that needs to be done.
#
# Another idea is to run the binary under Valgrind.

my $program_name= $0;
if ($0 =~ /^(.*)\.t$/) {
  my $name= $1;
  $name= './'. $name if ($name !~ m|/|);
  exec $name, @ARGV;
} else {
  die "Invalid wrapper filename '$0' (no trailing .t).";
}
