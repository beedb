/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Test pack_ptr and related stuff.
*/

#include <string.h>
#include <stdio.h>

#include "beedb.h"
#include "beedb_tester.h"
#include "format_macros.h"

#include "packed.h"
#include "bitcopy.h"


/* Test putbits() and getbits(). */
void
test1(beedb_tester *t)
{
  uint64_t buf[2000];
  pack_overwriteptr p1(buf);

  uint64_t total_bits= 0;
  for (uint64_t i= 1; i <= 64; i++)
  {
    p1.putbits(i, i);
    p1.putbits(i, 0);
    p1.putbits(i, ((uint64_t)1 << (i-1)) - i);
    p1.putbits(1, 1);
    total_bits+= i+i+i+1;
  }
  p1.flush();

  uint64_t used_bits= p1.used_bits();
  if (total_bits != used_bits)
    return t->nok(NOK_IDENT "wrong number of bits used, %" PRIu64
                  " instead of %" PRIu64, used_bits, total_bits);
  uint64_t used_words= p1.used_words();
  uint64_t total_words= (total_bits+63)>>6;
  if (total_words != used_words)
    return t->nok(NOK_IDENT "wrong number of words used, %" PRIu64
                  " instead of %" PRIu64, used_words, total_bits);

  pack_ptr p2(buf);
  for (uint64_t i= 1; i <= 64; i++)
  {
    uint64_t x= p2.getbits(i);
    if (x != i)
      return t->nok(NOK_IDENT "getbits() error (i=%" PRIu64 ")", i);
    x= p2.getbits(i);
    if (x != 0)
      return t->nok(NOK_IDENT "getbits() error (i=%" PRIu64 ")", i);
    x= p2.getbits(i);
    if (x != (((uint64_t)1 << (i-1)) - i))
      return t->nok(NOK_IDENT "getbits() error (i=%" PRIu64 " expected=%" PRIu64
                    ", got=%" PRIu64 ")", i, x, ((uint64_t)1 << (i-1)) - i);
    x= p2.getbits(1);
    if (x != 1)
      return t->nok(NOK_IDENT "getbits() error (i=%" PRIu64 ")", i);
  }

  t->ok();
}

/* Test pack() and unpack(). */
void
test2(beedb_tester *t)
{
  uint64_t buf[2000];
  pack_overwriteptr p1(buf);

  uint64_t total_bits= 0;
  for (uint64_t i= 1; i <= 64; i++)
  {
    p1.pack5(i);
    p1.pack5(0);
    p1.pack5(((uint64_t)1 << (i-1)) - i);
    p1.pack5(1);
  }
  p1.flush();

  pack_ptr p2(buf);
  for (uint64_t i= 1; i <= 64; i++)
  {
    uint64_t x= p2.unpack5();
    if (x != i)
      return t->nok(NOK_IDENT "unpack() error (i=%" PRIu64 ")", i);
    x= p2.unpack5();
    if (x != 0)
      return t->nok(NOK_IDENT "unpack() error (i=%" PRIu64 ")", i);
    x= p2.unpack5();
    if (x != (((uint64_t)1 << (i-1)) - i))
      return t->nok(NOK_IDENT "unpack() error (i=%" PRIu64 " expected=%" PRIu64
                    ", got=%" PRIu64 ")", i, x, ((uint64_t)1 << (i-1)) - i);
    x= p2.unpack5();
    if (x != 1)
      return t->nok(NOK_IDENT "unpack() error (i=%" PRIu64 ")", i);
  }

  t->ok();
}

/* Test bitcopy() (multiple versions). */
void
test3(void (*copier)(uint64_t *, uint64_t, uint64_t *, uint64_t, uint64_t),
      beedb_tester *t)
{
  const int SZ= 64;
  uint64_t buf[SZ/8];
  uint64_t misc_buf[SZ/8];
  unsigned char zeros[SZ];
  unsigned char ones[SZ];
  unsigned char *misc= (unsigned char *)misc_buf;
  for (int i= 0; i < SZ; i++)
    misc[i]= ((i+1)*4) & 0xff;
  memset(zeros, 0, SZ);
  memset(ones, 0xff, SZ);

  for (int i= 0; i < 2; i++)
  {
    for (int start= 0; start < (SZ-1); start++)
    {
      for (int len= 1; (start + len) <= SZ; len++)
      {
        unsigned char *check = (i ? zeros : ones);
        memcpy(buf, check, SZ);
        (*copier)(buf, start*8, misc_buf, (start/2)*8, len*8);
        if (start > 0)
        {
          if (0 != memcmp((unsigned char *)buf, check, start))
            return t->nok(NOK_IDENT "pre overwritten, i=%d start=%d len=%d",
                          i, start, len);
        }
        if (0 != memcmp((unsigned char *)buf + start, misc + (start/2), len))
          return t->nok(NOK_IDENT "bitstring wrong, i=%d start=%d len=%d",
                        i, start, len);
        int after= (start + len + 7) & ~7;
        if (after > 0)
        {
          if (0 != memcmp((unsigned char *)buf + after, check, SZ - after))
            return t->nok(NOK_IDENT "post overwritten, i=%d start=%d len=%d"
                          " (after=%d)", i, start, len, after);
        }
      }
    }
  }
  t->ok();
}


int
main(int argc, char *argv)
{
  beedb_tester t(5);
  t.run(test1);
  t.run(test2);
  t.run(test3, bitcopy2);
  t.run(test3, bitcopy);
  t.run(test3, bitcopy3);

  return 0;
}
