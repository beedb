CREATE TABLE perf_counts (
  test_run_id   INT NOT NULL,
  instance_idx  INT NOT NULL,
  counter_idx   INT NOT NULL,
  counter_name  VARCHAR(100) NOT NULL,
  counter_value BIGINT NOT NULL,
  PRIMARY KEY (test_run_id, instance_idx, counter_idx)
);

CREATE TABLE perf_wallclock (
  test_run_id   INT NOT NULL,
  instance_idx  INT NOT NULL,
  secs          DOUBLE NOT NULL,
  PRIMARY KEY (test_run_id, instance_idx)
);

CREATE TABLE perf_test_run (
  id            INT PRIMARY KEY AUTO_INCREMENT,
  suite_run_id  INT NOT NULL,
  /* Major is the test program, minor the test name. */
  test_major    VARCHAR(50) NOT NULL,
  test_minor    VARCHAR(50) NOT NULL,
  /* Different parameters (eg. buffer size), can be NULL. */
  param1        VARCHAR(200),
  param2        VARCHAR(200),
  /* Variants of a test are intended to be compared against each other. */
  /* Can be NULL if no variant. */
  variant       VARCHAR(50),
  /* Number of iterations run. */
  /* Used to compute cost/iteration. */
  iterations    BIGINT NOT NULL,
  /* Amount of work done in one iteration (eg. bytes copied etc), optional. */
  workunits     BIGINT
);

CREATE TABLE perf_suite_run (
  id        INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dt        TIMESTAMP NOT NULL,
  code_rev  VARCHAR(128),
  host_id   INT
);

CREATE TABLE perf_host (
  id        INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  hostname  VARCHAR(128),
  os        VARCHAR(64),
  kernel    VARCHAR(64),
  arch      VARCHAR(64),
  cpu_name  VARCHAR(128),
  cpu_mhz   DOUBLE
);
