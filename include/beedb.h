/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  This header contains general definitions used throughout all of the BeeDB
  source code, and should be included as the first non-system header in all
  BeeDB C++ source code.

  Note that because of this, this header should be kept fairly slim and should
  _not_ contain any stuff not of general interest to BeeDB code, ie. do not
  include definitions that are used only in one or a few modules.
*/

/* We need UINT64_MAX and friends. */
#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <stdint.h>

#include "port/beedb.h"


typedef unsigned int uint;


#ifdef __GNUC__

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#define NOINLINE __attribute__((noinline))

#else

#define likely(x) (x)
#define unlikely(x) (x)

#define NOINLINE
#endif

/*
  Preprocessor trick needed to convert number to string, eg. for putting
  __LINE__ into a string literal.
*/
#define MACRO_NUMBER_TO_STRING2(n) #n
#define MACRO_NUMBER_TO_STRING(n) MACRO_NUMBER_TO_STRING2(n)
