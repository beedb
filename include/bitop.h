/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/* ToDo: take this from autoconf instead. */
#if defined(__x86_64__)
#define SIZEOF_LONG 8
#define SIZEOF_LONG_LONG 8
#elif defined(__i386__)
#define SIZEOF_LONG 4
#define SIZEOF_LONG_LONG 8
#else
#error Unknown sizeof(long)
#endif

#ifdef __GNUC__

/*
  Count leading zero bits, compiler intrinsics to get access to CPU native
  instructions, if available.
*/
#if SIZEOF_LONG == 8
#define COUNT_LEADING_ZEROS_64(x) __builtin_clzl((x))
#elif SIZEOF_LONG_LONG == 8
#define COUNT_LEADING_ZEROS_64(x) __builtin_clzll((x))
#else
#error No 64-bit integer type available
#endif

#else
#error No COUNT_LEADING_ZEROS_64 defined for compiler
#endif

/*
  Non-temporal stores.

  These write directly to memory bypassing cache.
*/

#ifdef __GNUC__
#define STORE_NON_TEMPORAL_64(p, v) asm("movnti %1, %0" : "=m" (*(p)) : "r" (v))
//  __builtin_ia32_movntq((long long unsigned int*)(p), (long long unsigned)(v))
#else
#define STORE_NON_TEMPORAL_64(p, v) (*(uint64_t *)(p) = (uint64_t)(v))
#endif
