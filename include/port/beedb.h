/*
  Copyright 2008 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Header included in every BeeDB C++ source file, system specific stuff.
*/

#ifdef __GNUC__

#define NORETURN __attribute__ ((noreturn))
#define PRINTF_LIKE(format_idx, varargs_idx) \
  __attribute ((format (printf, format_idx, varargs_idx)))

#else

#define NORETURN
#define PRINTF_LIKE

#endif
