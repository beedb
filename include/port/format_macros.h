/*
  Copyright 2009 Kristian Nielsen

  This file is part of BeeDB.

  Foobar is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  Foobar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  The purpose of this file is to define the PRIu64 and similar macros
  for printf() format specifiers for stdint types.

  It is placed in port/ in case some platforms may not support them the
  standard C99 way, which apparently is to #define __STDC_FORMAT_MACROS prior
  to including inttypes.h.
*/

#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#ifndef __STDC_LIMIT_MACROS
#define __STDC_LIMIT_MACROS
#endif
#include <inttypes.h>
